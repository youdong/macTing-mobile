const TEST_MODE = true;

const TEST_SERVER_HOME = 'http://192.168.219.128:9999/';
const TEST_SERVER_CCLIM = 'http://172.30.1.3:9999/'
const TEST_SERVER_YEMIAN = 'http://192.168.0.35:9999/'
const TEST_SERVER_AWS = 'http://ec2-13-125-205-113.ap-northeast-2.compute.amazonaws.com:9999/'

const REAL_SEVER = '';
const TEST_SERVER = TEST_SERVER_AWS;

export const SERVER = TEST_MODE ? TEST_SERVER : REAL_SEVER;
