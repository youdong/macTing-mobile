import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';

// import BaseDrawerNavigator from 'src-root/view/common/navigator/BaseDrawerNavigator'
import store from 'src-root/store/store';
import MainView from 'src-root/view/MainView';

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <MainView/>
      </Provider>
    );
  }
}