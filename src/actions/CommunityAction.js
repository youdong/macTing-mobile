// action
//comunity
export const GET_TIMELINE = "GET_TIMELINE";
export const GET_DETAIL_CONTENT = "GET_DETAIL_CONTENT";
export const GET_MY_WRITING = "GET_MY_WRITING";
export const GET_FAVORITE_CONTENT = "GET_FAVORITE_CONTENT";
export const REG_CONTENT = "REG_CONTENT";
export const DEL_CONTENT = "DEL_CONTENT";
export const MOD_CONTENT = "MOD_CONTENT";
export const ADD_FAVORITE_CONTENT = "ADD_FAVORITE_CONTENT";
export const DEL_FAVORITE_CONTENT = "DEL_FAVORITE_CONTENT";

export const INIT_TIMELINE_LIST = "INIT_TIMELINE_LIST";
export const INIT_FAVORITE_CONTENT = "INIT_FAVORITE_CONTENT";
export const INIT_MY_WRITING = "INIT_MY_WRITING";

export const REG_PHOTO = "REG_PHOTO";
export const REG_REPLY = "REG_REPLY";

// comment
export const WRITE_COMMENT = "WRITE_COMMENT";
export const GET_COMMENT = "GET_COMMENT";

//action status
export const GET_TIMELINE_SUCCESS = "GET_TIMELINE_SUCCESS";
export const GET_TIMELINE_FAIL = "GET_TIMELINE_FAIL";
export const GET_DETAIL_CONTENT_SUCCESS = "GET_DETAIL_CONTENT_SUCCESS";
export const GET_DETAIL_CONTENT_FAIL = "GET_DETAIL_CONTENT_FAIL";
export const GET_MY_WRITING_SUCCESS = "GET_MY_WRITING_SUCCESS ";
export const GET_MY_WRITING_FAIL = "GET_MY_WRITING_FAIL";
export const GET_FAVORITE_CONTENT_SUCCESS = "GET_FAVORITE_CONTENT_SUCCESS";
export const GET_FAVORITE_CONTENT_FAIL = "GET_FAVORITE_CONTENT_FAIL";
export const REG_CONTENT_SUCCESS = "REG_CONTENT_SUCCESS";
export const REG_CONTENT_FAIL = "REG_CONTENT_FAIL";
export const DEL_CONTENT_SUCCESS = "DEL_CONTENT_SUCCESS";
export const DEL_CONTENT_FAIL = "DEL_CONTENT_FAIL";
export const MOD_CONTENT_SUCCESS = "MOD_CONTENT_SUCCESS";
export const MOD_CONTENT_FAIL = "MOD_CONTENT_FAIL";
export const ADD_FAVORITE_CONTENT_SUCCESS = "ADD_FAVORITE_CONTENT_SUCCESS";
export const ADD_FAVORITE_CONTENT_FAIL = "ADD_FAVORITE_CONTENT_FAIL";
export const DEL_FAVORITE_CONTENT_SUCCESS = "DEL_FAVORITE_CONTENT_SUCCESS";
export const DEL_FAVORITE_CONTENT_FAIL = "DEL_FAVORITE_CONTENT_FAIL";

export const WRITE_COMMENT_SUCCESS = "WRITE_COMMENT_SUCCESS";
export const WRITE_COMMENT_FAIL = "WRITE_COMMENT_FAIL";
export const GET_COMMENT_SUCCESS = "GET_COMMENT_SUCCESS";
export const GET_COMMENT_FAIL = "GET_COMMENT_FAIL";

export const REG_PHOTO_SUCCESS = "REG_PHOTO_SUCCESS";
export const REG_PHOTO_FAIL = "REG_PHOTO_FAIL";
export const REG_COMMENT_SUCCESS = "REG_COMMENT_SUCCESS";
export const REG_COMMENT_FAIL = "REG_COMMENT_FAIL";


// action creator
export function getTimeLine(pageNum) {
    return {
        types: [GET_TIMELINE_SUCCESS, GET_TIMELINE_FAIL],
        api: 'ting/community/timeline',
        params: pageNum
    }
}

export function initTimeLineList() {
    return {
        type:INIT_TIMELINE_LIST
    }
}

export function getFavoriteContent(params) {
    return {
        types: [GET_FAVORITE_CONTENT_SUCCESS, GET_FAVORITE_CONTENT_FAIL],
        api: 'ting/community/getFavoriteContent',
        params: params
    }
}

export function initFavoriteContentList() {
    return {
        type:INIT_FAVORITE_CONTENT
    }
}

export function getMyWriting(params) {
    return {
        types: [GET_MY_WRITING_SUCCESS, GET_MY_WRITING_FAIL],
        api: 'ting/community/myWritedContents',
        params: params
    }
}

export function initMyWritingList() {
    return {
        type:INIT_MY_WRITING
    }
}

export function regContent(uris, params, callback) {
    return {
        types: [REG_CONTENT_SUCCESS, REG_CONTENT_FAIL],
        api: 'ting/community/writeContent',
        image : uris,
        params: params,
        callback : callback
    }
}

export function deleteContent(contentInfo, callback) {
    return {
        types: [DEL_CONTENT_SUCCESS, DEL_CONTENT_FAIL],
        api:'ting/community/deleteContent',
        params: contentInfo,
        callback: callback,
    }
}

export function modifyContent(contentInfo, callback) {
    return {
        types: [MOD_CONTENT_SUCCESS, MOD_CONTENT_FAIL],
        api:'ting/community/modifiyCotent',
        params: contentInfo,
        callback: callback,
    }
}

export function addFavoriteContent(contentInfo, callback){
    return {
        types: [ADD_FAVORITE_CONTENT_SUCCESS, ADD_FAVORITE_CONTENT_FAIL],
        api:'ting/community/addFavoriteContent',
        params: contentInfo,
        callback: callback,
    }
}

export function delFavoriteContent(contentInfo, callback){
    return {
        types:[DEL_FAVORITE_CONTENT_SUCCESS, DEL_FAVORITE_CONTENT_FAIL],
        api:'ting/community/deleteFavoriteContent',
        params:contentInfo,
        callback:callback
    }
}

export function writeComment(commentInfo, callback){
    return {
        types : [WRITE_COMMENT_SUCCESS, WRITE_COMMENT_FAIL],
        api : 'ting/comment/writeComment',
        params : commentInfo,
        callback : callback,
    }
}

export function getComment(commentInfo, callback){
    return {
        types : [GET_COMMENT_SUCCESS, GET_COMMENT_FAIL],
        api : 'ting/comment/getContentCommentList',
        params : commentInfo,
        callback : callback,
    }
}


// export function getDetailContent(userInfo) {
//     return {
//         types: [GET_INTEREST_CONTENT_SUCCESS, GET_INTEREST_CONTENT_FAIL],
//         api: 'http://www.myasset.com/e xtern/mobile/data/nonftf',
//         params: userInfo
//     }
// }
// 
// export function regPhoto(userInfo) {
//     return {
//         types: [REG_PHOTO_SUCCESS, REG_PHOTO_FAIL],
//         url: 'http://www.myasset.com/extern/mobile/data/nonftf',
//         params: userInfo
//     }
// }
// // writingUserid,contentId,content
// export function regComment(commentInfo) {
//     return {
//         types: [REG_COMMENT_SUCCESS, REG_COMMENT_FAIL],
//         url: 'http://www.myasset.com/extern/mobile/data/nonftf',
//         params: commentInfo
//     }
// }


