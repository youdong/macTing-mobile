// action
export const LOGIN_DO_LOGIN = "LOGIN_DO_LOGIN";
export const LOGIN_DO_LOGIN_SUCCESS = "LOGIN_DO_LOGIN_SUCCESS";
export const LOGIN_DO_LOGIN_FAIL = "LOGIN_DO_LOGIN_FAIL";
export const LOGIN_DO_LOGOUT = "LOGIN_DO_LOGOUT";

export const LOGIN_SET_IP_ADDRESS = 'LOGIN_SET_IP_ADDRESS';
// export const LOGIN_GET_INTO_REGIST = "LOGIN_GET_INTO_REGIST";
// export const LOGIN_GET_OUT_REGIST = "LOGIN_GET_OUT_REGIST";

// export const REQUEST = "REQUEST";
// export const REQUEST_SUCCESS = "REQUEST_SUCCESS";
// export const REQUEST_FAIL = "REQUEST_FAIL";

// action creator
export function doLogin(loginInfo, callback) {
    return {
        types: [LOGIN_DO_LOGIN_SUCCESS, LOGIN_DO_LOGIN_FAIL],
        api : 'auth',
        params : loginInfo,
        callback : callback
    };
}

// export function doLogin(loginInfo) {
//     return {
//         type: LOGIN_DO_LOGIN,
//     };
// }

export function doLogout() {
    return {
        type: LOGIN_DO_LOGOUT
    };
}

export function setIpAddress(ipAddress) {
    return {
        type:LOGIN_SET_IP_ADDRESS,
        ipAddress:ipAddress
    };
}

// export function getIntoRegist(){
//     return {
//         type: LOGIN_GET_INTO_REGIST
//     }
// }

// export function getOutRegist(){
//     return {
//         type: LOGIN_GET_OUT_REGIST
//     }
// }