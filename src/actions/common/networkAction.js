//action
export const REQUEST_CALL = "REQUEST_CALL";
export const RECEIVE_CALL = "RECEIVE_CALL";

// action creator
export function requestCall() {
    return {
        type: REQUEST_CALL
    }
}

export function receiveCall() {
    return {
        type: RECEIVE_CALL
    }
}