// action
export const INTRO_FINISH_LOADING = "INTRO_FINISH_LOADING"
export const GRANTED_CONTACT_PERMISSION = "GRANTED_CONTACT_PERMISSION"
export const INTRO_SAVE_CONTACTS_INFO = "INTRO_SAVE_CONTACTS_INFO"

// action creator
export function finishApploading() {
    return{
        type:INTRO_FINISH_LOADING
    }
}

export function grantedContactPermission() {
    return {
        type:GRANTED_CONTACT_PERMISSION
    }
}

export function saveContactsInfo (contactInfo) {
    return {
        type:INTRO_SAVE_CONTACTS_INFO,
        contactInfo : contactInfo
    }
}