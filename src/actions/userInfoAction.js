// action
export const USER_REGIST_REGIST = "USER_REGIST_REGIST";
export const USER_INFO_ADD_PHOTO = "USER_INFO_ADD_PHOTO"
export const USER_INFO_DELETE_PHOTO = "USER_INFO_DELETE_PHOTO";
export const USER_INFO_GET_USER_INFO = "USER_INFO_GET_USER_INFO";

export const USER_INFO_SEND_IMAGE_TEST = "USER_INFO_SEND_IMAGE_TEST";

export const REGIST_USER_SUCCESS = "REGIST_USER_SUCCESS";
export const REGIST_USER_FAIL = "REGIST_USER_FAIL";

// action creator
export function registNewUser(userInfo, callback) {
    return{
        types: [REGIST_USER_SUCCESS, REGIST_USER_FAIL],
        api : 'ting/member/signUp',
        params: userInfo,
        callback : callback
    }
}

// mutlipart TEST
export function sendImageTest(uri, params){
    return{
        types: [REGIST_USER_SUCCESS, REGIST_USER_FAIL],
        api : 'ting/community/writeContent',
        image : uri,
        params : params
    }
}