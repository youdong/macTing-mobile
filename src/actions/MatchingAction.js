// action
export const MATCHING_SEARCH_MATCHING_INFO = "MATCHING_SEARCH_MATCHING_INFO"
export const MATCHING_SEARCH_PROFILE_INFO = "MATCHING_SEARCH_PROFILE_INFO"

export const MATCHING_INTRODUCE_TODAY = "MATCHING_INTRODUCE_TODAY"

export const MATCHING_INTRODUCE_TODAY_SUCCESS = "MATCHING_INTRODUCE_TODAY_SUCCESS"
export const MATCHING_INTRODUCE_TODAY_FAIL = "MATCHING_INTRODUCE_TODAY_FAIL"

export function searchMatchInfo() {
    return {
        type : MATCHING_SEARCH_MATCHING_INFO
    }
}

export function searchProfileInfo() {
    return {
        type : MATCHING_SEARCH_PROFILE_INFO
    }
}

export function getIntroduceToday() {
    return {
        types: [MATCHING_INTRODUCE_TODAY_SUCCESS, MATCHING_INTRODUCE_TODAY_FAIL],
        api : 'ting/match/introduce/today/',
    }
}