// action
export const PROFILE_INFO_COPY_USER_INFO = "PROFILE_INFO_COPY_USER_INFO";
export const PROFILE_INFO_ADD_PHOTO = "PROFILE_INFO_ADD_PHOTO"
export const PROFILE_INFO_DELETE_PHOTO = "PROFILE_INFO_DELETE_PHOTO";
export const PROFILE_INFO_REG_PROFILE = "PROFILE_INFO_REG_PROFILE";
export const PROFILE_INFO_MOD_PROFILE = "PROFILE_INFO_MOD_PROFILE";

export const PROFILE_INFO_GET_USER_INFO_SUCCESS = "PROFILE_INFO_GET_USER_INFO_SUCCESS";
export const PROFILE_INFO_GET_USER_INFO_FAIL = "PROFILE_INFO_GET_USER_INFO_FAIL";
export const PROFILE_INFO_REG_PROFILE_SUCCESS = "PROFILE_INFO_REG_PROFILE_SUCCESS";
export const PROFILE_INFO_REG_PROFILE_FAIL = "PROFILE_INFO_REG_PROFILE_FAIL";
export const PROFILE_INFO_MOD_PROFILE_SUCCESS = "PROFILE_INFO_MOD_PROFILE_SUCCESS";
export const PROFILE_INFO_MOD_PROFILE_FAIL = "PROFILE_INFO_MOD_PROFILE_FAIL";

export function getUserProfileInfo (id) {
    return {
        types : [PROFILE_INFO_GET_USER_INFO_SUCCESS, PROFILE_INFO_GET_USER_INFO_FAIL],
        api : 'ting/profile/0',
        params : id,
    }
}

export function registProfile (profileInfo, images, callback) {
    return {
        types : [PROFILE_INFO_REG_PROFILE_SUCCESS, PROFILE_INFO_REG_PROFILE_FAIL],
        api : 'ting/profile/register',
        params : profileInfo,
        image : images,
        callback : callback
    }
}

export function modifyProfile (profileInfo, images, callback) {
    return {
        types : [PROFILE_INFO_MOD_PROFILE_SUCCESS, PROFILE_INFO_MOD_PROFILE_FAIL],
        api : 'ting/profile/modify',
        params : profileInfo,
        image : images,
        callback : callback
    }
}

export function copyMyProfileInfo () {
    return {
        type: PROFILE_INFO_COPY_USER_INFO,
    }
}

export function addUserPhotoImage(num, photoImage){
    return{
        type:PROFILE_INFO_ADD_PHOTO,
        photoImage:photoImage,
        num : num
    }
}

export function deleteUserPhotoImage(num){
    return {
        type: PROFILE_INFO_DELETE_PHOTO,
        num: num
    }
}