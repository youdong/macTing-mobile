import * as types from '../actions/userInfoAction'

// state
const initialState = {
    userInfo:{
        memNm : '아이유',
        cpNo : '010-1234-5678',
        gender : 'F',
        age : '20대 중반',
        pictures:[
            // {
            //     uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
            // },
            // {
            //     uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
            // },
        ],
    },
};

// reducers
export default  (state = initialState, action) => {

    switch (action.type){
        case types.REGIST_USER_SUCCESS :
            return {
                ...state,
                userInfo : action.response.resultList[0]
            }
        case types.REGIST_USER_FAIL :
            return {
                ...state,
                userInfo : ""
            }

        
        default :
            return state
    }
}