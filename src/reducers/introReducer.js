import * as types from '../actions/introAction'

// state
const initialState = {
    isAppLoaded : false,
    grantedContactPermission : false
};

// reducer
export default (state = initialState, action) => {
    switch (action.type){
        case types.INTRO_FINISH_LOADING :
            return {
                ...state,
                isAppLoaded : true
            }
        case types.GRANTED_CONTACT_PERMISSION:
            return {
                ...state,
                grantedContactPermission : true
            }
        case types.INTRO_SAVE_CONTACTS_INFO:
            return {
                ...state,
                contactInfo : action.contactInfo
            }
        default :
            return state;
    }
}