import * as types from '../actions/MatchingAction'

// state
const initialState = {
    todayMatchingList:[],
    matchingList:[
        {
            key:1,
            uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
            memNm : '윤상필1',
            age : '30대 중반',
            ctNm : '서울',
            jobNm : '회사원',
            cmpNm: '동양네트웍스',
            rlgnNm: '힌두교',
            tag : '#초등학교 #교사 #운동',
            tags : ['초등학교', '교사', '운동', '필라테스'],
            isUser : true,
            countFriends : 5
        },
        {
            key:2,
            uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
            memNm : '윤상필',
            age : '30대 초반',
            ctNm : '제주도',
            jobNm : '자영업',
            cmpNm: '구멍가게',
            rlgnNm: '개신천지교',
            tag : '#활발 #애교',
            tags : ['활발', '애교'],
            isUser : false,
            countFriends : 2
        }
    ]
};

export default  (state = initialState, action) => {
    switch(action.type){
        case types.MATCHING_SEARCH_MATCHING_INFO :
            return{

            };
        case types.MATCHING_SEARCH_PROFILE_INFO :
            return{
            
            };
        case types.MATCHING_INTRODUCE_TODAY_SUCCESS :
            return{
                todayMatchingList:action.response.resultList
            }

        default :
            return state;
    }
}