import * as types from '../../actions/common/networkAction'

// state
const initialState = {
    isFetching : false,
    // isError : false,
};

// reducers
export default  (state = initialState, action) => {
    switch (action.type){
        case types.REQUEST_CALL:
            return {
                isFetching : true,
                // isError : false
            }
        case types.RECEIVE_CALL:
            return {
                isFetching : false,
                // isError : false
            }
        default :
            return state
    }
}