import * as types from '../actions/ProfileAction'

const initialState = {
    myProfile:{},
    myProfileTemp:{},

    profileInfo:{
        memNm : '윤상필1',
        age : '30대 중반',
        ctNm : '서울',
        jobNm : '회사원',
        cmpNm: '동양네트웍스',
        rlgnNm: '힌두교',
        tag : '#aaaa#bbbbbbbb#cccccccc',
        isUser : true,
        countFriends : 5,
        showImageTicker : 0,
        pictures:[
            'https://images.unsplash.com/photo-1441742917377-57f78ee0e582?h=1024',
            'https://images.unsplash.com/photo-1441716844725-09cedc13a4e7?h=1024',
            'https://images.unsplash.com/photo-1441448770220-76743f9e6af6?h=1024',
            'https://images.unsplash.com/photo-1441260038675-7329ab4cc264?h=1024',
            'https://images.unsplash.com/photo-1441126270775-739547c8680c?h=1024',
            'https://images.unsplash.com/photo-1440964829947-ca3277bd37f8?h=1024',
            'https://images.unsplash.com/photo-1440847899694-90043f91c7f9?h=1024'
        ],
        otherContents:[
            {
                 bodyType : '날씬',
                 style : '글래머',
                 character : '개썅'
             },
             {
                 bodyType : '늘씬',
                 style : '쭉빵',
                 character : '...'
             }
     ]
    }
}

export default  (state = initialState, action) => {
    switch(action.type){
        case types.PROFILE_INFO_GET_USER_INFO_SUCCESS : 
            return {
                ...state,
                myProfile : action.response.resultList[0]
            }
        case types.PROFILE_INFO_COPY_USER_INFO :
            return {
                ...state,
                myProfileTemp : state.myProfile
            }
        // case types.USER_INFO_GET_USER_INFO_FAIL : 
        //     return {
        //         ...state,
        //         userInfo : {}
        //     }
        case types.PROFILE_INFO_ADD_PHOTO :
            return {
                ...state,
                myProfileTemp : {
                    ...state.myProfileTemp,
                    pic1 : action.num === 1 ? action.photoImage : state.myProfileTemp.pic1,
                    pic2 : action.num === 2 ? action.photoImage : state.myProfileTemp.pic2,
                    pic3 : action.num === 3 ? action.photoImage : state.myProfileTemp.pic3, 
                    // pictures:[
                    //     ...state.userInfo.pictures,
                    //     action.photoImage
                    // ]
                }
            }
        case types.PROFILE_INFO_DELETE_PHOTO :
            return {
                ...state,
                myProfileTemp : {
                    ...state.myProfileTemp,
                    pic1 : "",//(action.num === 1 ? state.userInfoTemp.pic2 : state.userInfoTemp.pic1),
                    pic2 : (action.num === 2 ? state.myProfileTemp.pic3 : state.myProfileTemp.pic2),
                    pic3 : "",
                    // pictures:[
                    //     ...state.userInfo.pictures.slice(0, action.num),
                    //     ...state.userInfo.pictures.slice(action.num+1, state.userInfo.pictures.length)
                    // ]
                }
            }
        default :
            return state;
    }
}