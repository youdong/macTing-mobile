import * as types from '../actions/loginAction'

// state
const initialState = {
    isLogin : false,
    // getIntoRegist : false,
    token:"",
    ipAddress:"",
    userId:""
};

// reducers
export default  (state = initialState, action) => {
    console.log(action)
    switch (action.type){
        case types.LOGIN_DO_LOGIN :
            return {
                ...state,
                // isLogin : true
            };
        case types.LOGIN_DO_LOGIN_SUCCESS :
            return {
                ...state,
                isLogin : true,
                userId : action.request.username,
                token : action.response.token
                };
        case types.LOGIN_DO_LOGOUT :
            return {
                ...state,
                isLogin : false
            };
        case types.LOGIN_SET_IP_ADDRESS :
            return {
                ...state,
                ipAddress:action.ipAddress
            }
        // case types.LOGIN_GET_INTO_REGIST :
        //     return {
        //         ...state,
        //         getIntoRegist : true
        //     };
        // case types.LOGIN_GET_OUT_REGIST :
        //     return {
        //         ...state,
        //         getIntoRegist : false
        //     };

        default :
            return state;
    }
}