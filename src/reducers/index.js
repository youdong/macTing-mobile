import { combineReducers } from 'redux';

import login from './loginReducer';
import userInfo from './userInfoReducer';
import network from './common/networkReducer';
import intro from './introReducer';
import community from './CommunityReducer';
import matching from './MatchingReducer';
import myTing from './MyTingReducer';
import profile from './ProfileReducer';

const reducers = combineReducers({
    login,
    userInfo,
    network,
    intro,
    community,
    matching,
    myTing,
    profile
});

export default reducers;

