import * as types from '../actions/MyTingAction'

const initialState = {
    // 나를 좋아하는 카드 리스트
    cardsWhoLikeMe:[
        // {
        //     memNm:'테스트',
        //     memAge:'20대 후반',
        //     pic1:'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //     pictures:[
        //         {
        //             uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         },
        //         // {
        //         //     uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         // }, 
        //     ]
        // },
        // {
        //     memNm:'테스트',
        //     memAge:'20대 후반',
        //     pic1:'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //     pictures:[
        //         {
        //             uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         },
        //         // {
        //         //     uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         // }, 
        //     ]
        // },
        // {
        //     memNm:'테스트',
        //     memAge:'20대 후반',
        //     pic1:'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //     pic1:'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //     pictures:[
        //         {
        //             uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         },
        //         // {
        //         //     uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         // }, 
        //     ]
        // },
    ],
    // 내가 좋아하는 카드 리스트
    cardsWhoILike:[
        // {
        //     memNm:'테스트',
        //     memAge:'20대 후반',
        //     pic1:'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //     pictures:[
        //         {
        //             uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         },
        //         // {
        //         //     uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         // }, 
        //     ]
        // },
    ],
    // 주선을 요청한 카드 리스트
    cardsWhoIAsk:[
        // {
        //     memNm:'테스트',
        //     memAge:'20대 후반',
        //     pic1:'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //     pictures:[
        //         {
        //             uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         },
        //         // {
        //         //     uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         // }, 
        //     ]
        // },
        // {
        //     memNm:'테스트',
        //     memAge:'20대 후반',
        //     pic1:'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //     pictures:[
        //         {
        //             uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         },
        //         // {
        //         //     uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         // }, 
        //     ]
        // },
    ],
    // 관심 표현을 한 카드 리스트
    cardsWhoIInterest:[
        // {
        //     memNm:'테스트',
        //     memAge:'20대 후반',
        //     pic1:'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //     pictures:[
        //         {
        //             uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         },
        //         // {
        //         //     uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg',
        //         // }, 
        //     ]
        // },
    ]
}

export default  (state = initialState, action) => {
    switch(action.type){

        default :
            return state;
    }
}