import * as types from '../actions/CommunityAction'

// state
const initialState = {
    
    timeLineList:[],
    timeLinePageFetch:true,
    favoriteContentList:[],
    favoriteContentPageFetch: true,
    myWritingList:[],
    myWritingPageFetch: true
};

// reducers
export default (state = initialState, action) => {
    switch (action.type) {
        // 타임라인 (리스트)
        case types.GET_TIMELINE_SUCCESS :
            return {
                ...state,
                timeLineList: [
                    ...state.timeLineList,
                    ...action.response.resultList.slice(0, action.response.resultList.length)
                ],
                timeLinePageFetch: action.response.resultList.length === 0 ? false : true
            }
        case types.GET_TIMELINE_FAIL :
            return {
                ...state,
                timeLineList: []
            }
        case types.INIT_TIMELINE_LIST :
            return {
                ...state,
                timeLineList: []
            }
        // 컨텐츠 상세 보기
        // case types.GET_DETAIL_CONTENT_SUCCESS :
        //     return {
        //         ...state,
        //         response: action.response
        //     }
        // case types.GET_DETAIL_CONTENT_FAIL :
        //     return {
        //         ...state,
        //         response: action.response
        //     }
        // 내가 쓴 글 (리스트)
        case types.GET_MY_WRITING_SUCCESS :
            return {
                ...state,
                myWritingList: [
                    ...state.myWritingList,
                    ...action.response.resultList.slice(0, action.response.resultList.length),
                ],
                myWritingPageFetch: action.response.resultList.length === 0 ? false : true
            }
        case types.GET_MY_WRITING_FAIL :
            return {
                ...state,
                myWritingList: []
            }
        case types.INIT_MY_WRITING :
            return {
                ...state,
                myWritingList: []
            }
        // 내 관심 글 (리스트)
        case types.GET_FAVORITE_CONTENT_SUCCESS :
            return {
                ...state,
                favoriteContentList: [
                    ...state.favoriteContentList,
                    ...action.response.resultList.slice(0, action.response.resultList.length)
                ],
                favoriteContentPageFetch: action.response.resultList.length === 0 ? false : true
            }
        case types.GET_FAVORITE_CONTENT_FAIL :
            return {
                ...state,
                favoriteContentList: []
            }
        case types.INIT_FAVORITE_CONTENT :
            return {
                ...state,
                favoriteContentList: []
            }
        // 관심글 추가
        case types.ADD_FAVORITE_CONTENT_SUCCESS :
            return {
                ...state,
            }
        case types.ADD_FAVORITE_CONTENT_FAIL :
            return {
                ...state,
            }
        // 관심글 삭제
        case types.DEL_FAVORITE_CONTENT_SUCCESS :
            return {
                ...state,
            }
        case types.DEL_FAVORITE_CONTENT_FAIL :
            return {
                ...state,
            }
        // 글쓰기
        case types.REG_CONTENT_SUCCESS :
            return {
                ...state,
                // response: action.response
            }
        case types.REG_CONTENT_FAIL :
            return {
                ...state,
            }
        // 댓글 달기
        case types.WRITE_COMMENT_SUCCESS : 
            return {
                ...state,
            }
        case types.GET_COMMENT_SUCCESS :
            result = {
                ...state,
                timeLineList:
                    state.timeLineList.map((item, index) => {
                        return (item.postNo === action.request.postNo ?
                                {
                                    ...item,
                                    comments:action.response.resultList
                                }
                                : item)
                    }),
                
                favoriteContentList:
                    state.favoriteContentList.map((item, index) => {
                        return (item.postNo === action.request.postNo ?
                                {
                                    ...item,
                                    comments:action.response.resultList
                                }
                                : item)
                    }),
                
                myWritingList:
                    state.myWritingList.map((item, index) => {
                        return (item.postNo === action.request.postNo ?
                                {
                                    ...item,
                                    comments:action.response.resultList
                                }
                                : item)
                    })
            }

            return result;
        case types.WRITE_COMMENT_FAIL :
            return {
                ...state,
            }



        //
        case types.REG_PHOTO_SUCCESS :
            return {
                ...state,
                response: action.response
            }
        case types.REG_PHOTO_FAIL :
            return {
                ...state,
            }
        case types.REG_COMMENT_SUCCESS :
            return {
                ...state,
                response: action.response
            }
        case types.REG_COMMENT_FAIL :
            return {
                ...state,
            }
        default :
            return state
    }
}