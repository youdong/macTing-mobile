import React, {Component} from 'react';
import {View, 
    Text, 
    Image, 
    StyleSheet, 
    TouchableHighlight, 
    Button, 
    Dimensions,
    ScrollView
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import {BaseComponent} from 'src-root/view/common/component/BaseComponent'
import CommunityCard from './CommunityCard'
import {commonStyles} from 'src-root/style/CommonStyle'
import {CommonModal} from 'src-root/view/common/component/CommonModal'

import { connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions';

class MyWriting extends BaseComponent {

    constructor(props){
        super(props);
        this.state = {
            pageNum:1
        }

        this._selectedTab = this._selectedTab.bind(this);
    }

    _selectedTab = () => {
        console.log('my writing Tab selected Tab');
    }

    componentWillMount(){
        // this._init();
    }

    _init = () => {
        if(this.props.myWritingList.length > 0) 
            this.props.initMyWriting();

        params = {
            pageNum:1
        }
        this.props.getMyWriting(params);
    }

    _onListenFromCommunityCard = (listenType, params) => {
        switch(listenType){
            case 1:         // deleteContent
            {
                this._init();
                return;
            }
            case 2:
            {
                this.setState({contentInfo:params});
                this._showModal();
            }
        }
    }

    _onScrollEnd() {
        if (this.props.myWritingPageFetch) {
            this.setState({pageNum:++this.state.pageNum})
        }

        params = {
            pageNum:this.state.pageNum
        }
        this.props.getMyWriting(params);
    }

    _isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {

        if (layoutMeasurement.height + contentOffset.y >=
            contentSize.height-10) return true;
        else return false;
      };

    _getModal = () => {
        var btn = [
            {
                func : () => this._modifyContent(),
                btnNm : "수정하기"
            },
            {
                func : () => this._deleteContent(),
                btnNm : "삭제하기"
            }
        ];

        return (<CommonModal ref="modal" button = {btn}/>)
    }

    _modifyContent = () => {
        
        this.props.navigation.navigate('Write', {
            postCnte:this.state.contentInfo.postCnte,
            imageUri1:this.state.contentInfo.pic1,
            postNo:this.state.contentInfo.postNo,
            isModify:true
        });

        this._dismissModal();
    }

    _deleteContent = () => {
        var callback = () => this._init();

        this.props.deleteContent({postNo:this.state.contentInfo.postNo}, callback);
        
        this._dismissModal();
    }

    _showModal = () => {
        this.refs['modal'].showModal();
    }

    _dismissModal = () => {
        this.refs['modal'].dismissModal();
    }

    render() {
        const myWritingList = this.props.myWritingList;

        return (
            <View style={{flex:1}}>
                {myWritingList.length === 0 ? 
                    <View style={styles.blankStyle}> 
                        <Text> 리스트가 없습니다. </Text>
                    </View>:
                    <View style={{flex:1, backgroundColor:'white'}}>
                        {this._getModal()}
                        <KeyboardAwareScrollView bounces={false} onMomentumScrollEnd={({nativeEvent}) => {
                                        if (this._isCloseToBottom(nativeEvent)) {
                                            this._onScrollEnd();
                                        }
                                    }}>
                            {this.props.myWritingList.map((item, idx) => {
                                return <CommunityCard
                                        callAction = {this._onListenFromCommunityCard.bind(this)} 
                                        item={item} key={idx} navigation={this.props.navigation}/>})
                            }
                        </KeyboardAwareScrollView>
                    </View>
                }
            </View>
        );
    }
}
const styles = StyleSheet.create({
    blankStyle:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: 'white'
    },
    contentContainer: {
        height: 150,
        backgroundColor: 'white',
        marginBottom: 5,
    },
    contentContainerTop: {
        height: 50, flexDirection: 'row', alignItems: 'center'
    },
    contentContainerBottom: {
        height: 50, flexDirection: 'row'
    },
    contentLeft: {
        width: 100
    },
    contentRight: {
        flex: 1, marginLeft: 15
    },
    contentBottom: {
        width: 100,
        marginLeft: 10
    },
    contentText: {
        color: 'gray', fontSize: 15
    },
    contentImg: {
        margin: 3, marginTop: 0, width: 100, height: 100
    }
});

const mapStateToProps = (state) => {
    
        return {
            myWritingList: state.community.myWritingList,
            myWritingPageFetch: state.community.myWritingPageFetch
        };
    };
    
    const mapDispatchProps = (dispatch) => {
        return {
            getMyWriting: (params) => {dispatch(actions.getMyWriting(params))},
            initMyWriting: () => {dispatch(actions.initMyWritingList())},
            deleteContent: (contentInfo, callback) => {dispatch(actions.deleteContent(contentInfo, callback))},
            // getIntoRegist: () => {dispatch(actions.getIntoRegist())}
        };
    };
    
    export default connect(mapStateToProps, mapDispatchProps)(MyWriting)