import React, { Component } from 'react';
import { View, 
    Text, 
    Image, 
    StyleSheet, 
    TouchableHighlight,
    ScrollView,
    Modal
 } from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import {BaseComponent} from 'src-root/view/common/component/BaseComponent'
import {CommonFunc} from 'src-root/view/common/component/CommonFunc'
import CommunityCard from './CommunityCard'
import {commonStyles} from 'src-root/style/CommonStyle'
import {CommonModal} from 'src-root/view/common/component/CommonModal'

import { connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions';

class TimeLine extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            pageNum:1,
        }

        // this._selectedTab = this._selectedTab.bind(this);
    }

    componentWillMount(){
        // this._init();
    }

    _onListenFromCommunityCard = (listenType, params) => {
        switch(listenType){
            case 1:         // deleteContent
            {
                this._init();
                return;
            }
            case 2:
            {
                this.setState({contentInfo:params});
                this._showModal();
            }
        }
    }

    _init = () => {
        if(this.props.timeLineList.length > 0) 
        this.props.initTimeLine();

        params = {
            pageNum:1
        }
        this.props.getTimeLine(params);
    }

    // _onPressButton(id) {
    //    this.props.navigation.navigate('DetailContent', { item: this.props.item});
    // };

    _onScrollEnd() {
        if (this.props.timeLinePageFetch) {
            this.setState({pageNum:++this.state.pageNum})
        }

        params = {
            pageNum:this.state.pageNum
        }
        this.props.getTimeLine(params);
    };

    _isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {

        if (layoutMeasurement.height + contentOffset.y >=
            contentSize.height-10) return true;
        else return false;
      };

    _getModal = () => {
        var btn = [
            {
                func : () => this._addFavoriteContent(),
                btnNm : "관심 등록"
            }
        ];

        return (<CommonModal ref="modal" button = {btn}/>)
    }

    _addFavoriteContent = () => {
        const params = {
            postNo:this.state.contentInfo.postNo,
        } 
        
        const callback = () => console.log("add favorite content callback!!");
        this.props.addFavoriteContent(params, callback);
        this._dismissModal();
    }

    _showModal = () => {
        if (this.refs['modal'])
            this.refs['modal'].showModal();
    }

    _dismissModal = () => {
        if (this.refs['modal'])
            this.refs['modal'].dismissModal();
    }

    render(){
        return(
            <View style={{flex:1}}>
                {this.props.timeLineList && this.props.timeLineList.length === 0 ? 
                    <View style={style.blankStyle}> 
                        <Text> 리스트가 없습니다. </Text>
                    </View>:
                    <View style={{flex:1, backgroundColor:'white'}}>
                        {this._getModal()}
                        <KeyboardAwareScrollView bounces={false} onMomentumScrollEnd={({nativeEvent}) => {
                                        if (this._isCloseToBottom(nativeEvent)) {
                                            this._onScrollEnd();
                                        }
                                    }}
                                    resetScrollToCoords={{ x: 0, y: 0 }}>
                            {this.props.timeLineList.map((item, idx) => {
                                return <CommunityCard
                                        callAction = {this._onListenFromCommunityCard.bind(this)} 
                                        item={item} key={idx} navigation={this.props.navigation}/>})
                            }
                        </KeyboardAwareScrollView>
                    </View>
                }
            </View>
        );
    }
}
const style = StyleSheet.create({
    blankStyle:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: 'white'
    },
    contentContainer: {
        height: 200,
        backgroundColor: 'white',
        marginBottom:5,
    },
    contentContainerTop: {
        height: 150, flexDirection: 'row', alignItems: 'center'
    },
    contentContainerBotom: {
        height: 50, flexDirection: 'row'
    },
    contentLeft: {
        width: 100
    },
    contentRight: {
        flex: 1, marginLeft: 15
    },
    contentBottom: {
        width: 100,
        marginLeft: 10
    },
    contentText: {
        color: 'gray', fontSize: 15
    },
    contentImg: {
        margin: 3, marginTop:0, width: 100, height: 100
    }
});


const mapStateToProps = (state) => {
    
        return {
            timeLineList: state.community.timeLineList,
            timeLinePageFetch: state.community.timeLinePageFetch
        };
    };
    
    const mapDispatchProps = (dispatch) => {
        return {
            getTimeLine: (pageNum) => {dispatch(actions.getTimeLine(pageNum))},
            initTimeLine : () => {dispatch(actions.initTimeLineList())},
            addFavoriteContent : (contentInfo, callback) => {dispatch(actions.addFavoriteContent(contentInfo, callback))},
        };
    };
    
export default connect(mapStateToProps, mapDispatchProps)(TimeLine)