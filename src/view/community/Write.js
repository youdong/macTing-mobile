import React, { Component } from 'react';
import {View, 
    Text, 
    Image, 
    StyleSheet, 
    TextInput, 
    TouchableHighlight, 
    Button, 
    Dimensions, 
    ScrollView,
    Alert,
    Keyboard
} from 'react-native';
import {BaseComponent} from 'src-root/view/common/component/BaseComponent'
import ImageUtil from 'src-root/util/ImageUtil'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions';

import HeaderRight from 'src-root/view/common/navigator/NavigatorHeaderRight';

const registBtnHeight = 50;
const writeTextInputHeight = 200;
// const imageHeight = Dimensions.get('window') - 60;

var contentHeight

class Write extends BaseComponent {

    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel : '글쓰기',
            title : '글쓰기',

            headerRight: <HeaderRight 
                    height={19}
                    // icon={"list"}
                    text = {"완료"}
                    action={
                        () => {navigation.state.params.sendNewContent()}
                    }
                />
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            keyboardSpace:0,
            nicknameModalVisible:true,
            postCnte:"",
            imageUri1 : ""
        };
        
        //for get keyboard height
        Keyboard.addListener('keyboardDidShow',(frames)=>{
            if (!frames.endCoordinates) return;
            this.setState({keyboardSpace: frames.endCoordinates.height});
        });
        Keyboard.addListener('keyboardDidHide',(frames)=>{
            this.setState({keyboardSpace:0});
        });
    }

    componentDidMount() {
        this.props.navigation.setParams({ sendNewContent: this._sendNewContent });
      }

    componentWillMount() {
        const navi = this.props.navigation;

        if (navi && navi.state && navi.state.params)
            this.setState({isModify:navi.state.params.isModify})
        if (navi && navi.state && navi.state.params && navi.state.params.postNo)
            this.setState({postNo:navi.state.params.postNo});
        if (navi && navi.state && navi.state.params && navi.state.params.postCnte)
            this.setState({postCnte:navi.state.params.postCnte});
        if (navi && navi.state && navi.state.params && navi.state.params.imageUri1)
            this.setState({imageUri1:navi.state.params.imageUri1});   
    }

    componentWillUnmount(){
        Keyboard.removeAllListeners('keyboardDidShow');
        Keyboard.removeAllListeners('keyboardDidHide');
    }

    // constructor(props){
    //     super(props);
    //     this.state = {
    //         // postTit:'test title',
    //         postCnte:"", 
    //         // regId:'111',
    //         // postNo:'4',
    //         // title:'',
    //         // content:'',
    //         imageUri1 : "",
    //         // imageUri2 : ImageUtil.getPlusImage(),
    //         // imageUri3 : ImageUtil.getPlusImage()
    //     }
    // }

    _setPhotoImage = (func) => {
        if(this.state.imageUri1){
            Alert.alert(
                'Alert Title',
                'delete image?',
                [
                  {text: 'Cancel'},
                  {text: 'OK', onPress: () => {this.setState({imageUri1:""})}},
                ],
                { cancelable: false }
            )
        }else{
            Alert.alert(
                'Alert Title',
                '_setPhotoImage',
                [
                  {text: 'Cancel'},
                  {text: 'OK', onPress: func},
                ],
                { cancelable: false }
            )
        }
    }

    _showPopup = (text) => {
        Alert.alert(
            'Alert Title',
            text,
            [
              {text: 'Cancel'},
            ],
            { cancelable: false }
          )
    }

    _sendNewContent = () => {
        // if (this.state.postTit === ''){
        //     this._showPopup('write title!!')
        //     return;
        // }
        if (!this.state.postCnte){
            this._showPopup('write content!!')
            return;
        }

        var callback = () => {
            this.props.navigation.goBack();
        }

        if (this.state.isModify){
            params = {
                postCnte:this.state.postCnte,
                postNo:this.state.postNo,
                // postTit:this.state.postTit
            };

            this.props.modifyContent(params, callback);
        }   
        else{
            uris = {
                imageUri1: ImageUtil.checkImageUri(this.state.imageUri1),
            };
    
            params = {
                postCnte:this.state.postCnte,
                // postTit:this.state.postTit
            };

            this.props.writeContent(uris, params, callback);
        }
            
    }

    _getHeightRegistButtion = () => {
        return ({
            width:Dimensions.get('window').width- 60,
            height:(this.state.imageUri1 ? 0 : registBtnHeight),
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'rgba(145,199,219,0.7)'
        })
    }

    _getHeightImage = () => {
        return ({
            width:Dimensions.get('window').width- 60,
            height: (this.state.imageUri1 ? Dimensions.get('window').width- 60 : 0)
            // margin: 10, marginTop:0, width: 100, height: 100,
        })
    }

    _getHeightWriteTextInput = () => {
        return ({
            width: Dimensions.get('window').width - 40,
            height: (this.state.imageUri1 ? writeTextInputHeight + Dimensions.get('window').width- 60 : writeTextInputHeight + registBtnHeight), 
            margin : 20,
            borderWidth: 0.5,
            borderColor:'rgba(0,0,0,0.2)',
            padding: 10,
            // borderColor:'rgba(10,10,10,0.5)'
            // marginLeft:10, marginRight:10
        })
    }

    render(){

        return(
            <KeyboardAwareScrollView bounces={false} style={[styles.contentContainer, {marginBottom:this.state.keyboardSpace}]}>
            {/* <Button style={{width:100, height:30}}
                            onPress = {() => {this._sendNewContent()}}
                            title="글쓰기 제출 버튼!!"
                            color="#841584"
                            accessibilityLabel="purple button"
                            /> */}
                {/*text box*/}
                {/* <View style={{width:'100%', height: 50, backgroundColor:'#AAAAAA'}}>
                    <TextInput
                        editable = {true}
                        maxLength = {2000}
                        multiline = {true}
                        underlineColorAndroid='transparent'
                        onChangeText = {(text) => this.setState({postTit: text})}
                        placeholder = {'제목을 입력해주세요.'}/>
                </View> */}
                <View style={[this._getHeightWriteTextInput()]}>
                    <View>
                        <TouchableHighlight onPress = {() => this._setPhotoImage(() => ImageUtil.pickImageFromGalary((uri) => {this.setState({imageUri1:uri})}))} style = {styles.contentImg}>
                            {this.state.imageUri1 ? 
                                <Image
                                    source={{uri: this.state.imageUri1}}
                                    style={this._getHeightImage()}/>
                            :   <View style= {this._getHeightRegistButtion()}>
                                    <Text style={styles.registPhotoText}> 사진 등록 </Text>
                                </View>
                            }
                            
                        </TouchableHighlight>
                    </View>

                    <TextInput
                        editable = {true}
                        maxLength = {2000}
                        multiline = {true}
                        underlineColorAndroid='transparent'
                        onChangeText = {(text) => this.setState({postCnte: text})}
                        numberOfLines = {0}
                        value = {this.state.postCnte}
                        style = {{marginTop:10, width: Dimensions.get('window').width - 60, height:writeTextInputHeight - 10}}
                        underlineColorAndroid="transparent"
                        placeholder = {'내용을 입력해주세요. 연락처 및 개인신상 정보 기재시 영구정지 됩니다.'}/>
                </View>
                {/*image*/}
                <View style={styles.contentContainerBottom}>
                    {/* <View>
                        <TouchableHighlight onPress = {() => this._setPhotoImage(() => ImageUtil.pickImageFromGalary((uri) => {this.setState({imageUri1:uri})}))} style = {styles.contentImg}>
                            {this.state.imageUri1 ? 
                                <Image
                                    source={{uri: this.state.imageUri1}}
                                    style={styles.contentImg}/>
                            :   <Image
                                    source={require('asset/plus.png')}
                                    style={styles.contentImg}/>
                            }
                            
                        </TouchableHighlight>
                    </View> */}
                    {/* <View>
                        <TouchableHighlight onPress = {() => this._setPhotoImage(() => ImageUtil.pickImageFromGalary((uri) => {this.setState({imageUri2:uri})}))} style = {styles.contentImg}>
                            {this.state.imageUri2 ? 
                                <Image
                                    source={{uri: this.state.imageUri2}}
                                    style={styles.contentImg}/>
                            :   <Image
                                    source={require('asset/plus.png')}
                                    style={styles.contentImg}/>
                            }
                        </TouchableHighlight>
                    </View>
                    <View>
                        <TouchableHighlight onPress = {() => this._setPhotoImage(() => ImageUtil.pickImageFromGalary((uri) => {this.setState({imageUri3:uri})}))} style = {styles.contentImg}>
                            {this.state.imageUri3 ? 
                                <Image
                                    source={{uri: this.state.imageUri3}}
                                    style={styles.contentImg}/>
                            :   <Image
                                    source={require('asset/plus.png')}
                                    style={styles.contentImg}/>
                            }
                        </TouchableHighlight>
                    </View> */}
                    {/*tag*/}
                    {/* <View style={styles.contentBottom}>
                        <Text style={styles.tagText}>1234</Text>
                    </View> */}
                </View>

            </KeyboardAwareScrollView>
        );
    }
}
const styles = StyleSheet.create({
    contentContainer: {
        height: Dimensions.get('window').height-50,
        backgroundColor: 'white',
    },
    contentContainerTop: {
        width: Dimensions.get('window').width - 40,
        // height: (this.state.imageUri1 ? writeTextInputHeight + imageHeight : writeTextInputHeight + registBtnHeight), 
        margin : 20,
        borderWidth: 0.5,
        borderColor:'rgba(0,0,0,0.2)',
        padding: 10
        // borderColor:'rgba(10,10,10,0.5)'
        // marginLeft:10, marginRight:10
    },
    contentContainerBottom: {
        height: 50, flexDirection: 'row'
    },
    contentLeft: {
        width: 100
    },
    contentRight: {
        flex: 1, marginLeft: 15
    },
    contentBottom: {
        width: 100,
        marginLeft: 10
    },
    contentText: {
        width:Dimensions.get('window').width, height: 150, 
        //alignment:'top'
    },
    registPhoto:{
        width:Dimensions.get('window').width- 60,
        // height:(this.state.imageUri1 ? 0 : registBtnHeight),
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'rgba(145,199,219,0.7)'
    },
    registPhotoText:{
        color:'white',
        fontWeight:'bold'
    },
    tagText: {
        color: 'gray', fontSize: 15
    },
    contentImg: {
        width:Dimensions.get('window').width- 60,
        // height: (this.state.imageUri1 ? imageHeight : 0)
        // margin: 10, marginTop:0, width: 100, height: 100,
    }
});

const mapStateToProps = (state) => {
    
    return {
        timeLineList: state.community.timeLineList
    };
};

const mapDispatchProps = (dispatch) => {
    return {
        writeContent: (uris, params, callback) => {dispatch(actions.regContent(uris, params, callback))},
        modifyContent: (params, callback) => {dispatch(actions.modifyContent(params, callback))},
    };
};

export default connect(mapStateToProps, mapDispatchProps)(Write)