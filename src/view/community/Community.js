import React, {Component} from 'react'
import {View, Text, StyleSheet, ScrollView, Dimensions, TextInput, Button, Image} from 'react-native'

import TabView from 'src-root/view/common/component/TabView'
import TabItem from 'src-root/view/common/component/TabItem'

import TimeLine from './TimeLine'
import InterestContent from './InterestContent'
import MyWriting from './MyWriting'
import Write from './Write'
import * as actions from "../../actions";
import {connect} from "react-redux";

class Community extends React.Component {
    constructor(props) {
        super(props);
    }

    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel: '커뮤니티',
            title: '커뮤니티',
            tabBarOnPress: (scene, jumpToIndex) => {
                if (!scene.focused){
                    jumpToIndex(scene.index);

                    const navigationInRoute = scene.route;
                    if(!!navigationInRoute && !!navigationInRoute.params && !!navigationInRoute.params.selectedTab)
                        navigation.state.params.selectedTab();
                }
            },
        }
    }

    _selectedTab = () => {
        this.refs['child']._selectedTab(0);
    }
    
    _onSelectTab = (index) => {
        switch(index){
            case 0:
                if(this.props.timeLineList.length > 0) 
                this.props.initTimeLine();

                params = {
                    pageNum:1
                }
                this.props.getTimeLine(params);
                break;
            case 1:
                if(this.props.myWritingList.length > 0) 
                this.props.initMyWriting();

                params = {
                    pageNum:1
                }
                this.props.getMyWriting(params);
                break;
            case 2:
                if(this.props.favoriteContentList.length > 0) 
                this.props.initFavoriteContent();

                params = {
                    pageNum:1
                }
                this.props.getFavoriteContent(params);
                break;
        }
    }

    componentWillMount(){
        // 시간차를 주어서 세팅... 딱히 맘에 들지는 않는다 ㅠㅠㅠ
        setTimeout(() => {
            this.props.navigation.setParams({selectedTab: this._selectedTab.bind(this)});
          }, 1)
    }

    render() {
        
        return (
            <View style={{flex: 1}}>
                {/* <View style={{  flexDirection: 'row'}}>
                    <View style={{width: Dimensions.get('window').width-50}}>
                        <TextInput
                            style={{height: 40, borderColor: 'gray', borderWidth: 0}}
                            onChangeText={(text) => this.setState({text})}
                        />
                    </View>
                    <View style={{width: 50}}>
                        <Button
                            title="검색"
                            color="#841584"
                            accessibilityLabel="purple button"
                            onPress={this.props.getTimeLine}
                        />
                    </View>
                </View> */}

                <TabView ref="child" onSelectTab = {this._onSelectTab}>
                    <TabItem title="타임라인">
                        <TimeLine navigation = {this.props.navigation} timeLineList = {this.props.timeLineList}/>
                    </TabItem>
                    <TabItem title="나의 관심글">
                        <InterestContent navigation = {this.props.navigation}/>
                    </TabItem >
                    <TabItem title="내가 쓴 글">
                        <MyWriting navigation = {this.props.navigation}/>
                    </TabItem>
                </TabView>
            </View>
        );
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'gray',
        flexDirection: 'column'
    },
    communityNavContainer: {
        height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: 'white'
    },
    communityNav: {
        flex: 1,
        height: 50,
        flexDirection: 'row',
        borderWidth: 0.5,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: 'gray',
        alignItems: 'center',
        justifyContent: 'center'
    },
    containerTab: {
        flex: 1,
    },
    tabbar: {
        backgroundColor: '#222',
    },
    tab: {
        width: 120,
    },
    indicator: {
        backgroundColor: '#ffeb3b',
    },
    label: {
        color: '#fff',
        fontWeight: '400',
    },
});


const mapStateToProps = (state) => {
    return {
        isLogin: state.login.isLogin,
        timeLineList: state.community.timeLineList,
        myWritingList: state.community.myWritingList,
        favoriteContentList: state.community.favoriteContentList,
    };
};

const mapDispatchProps = (dispatch) => {
    return {
        getTimeLine: (pageNum) => {dispatch(actions.getTimeLine(pageNum))},
        initTimeLine : () => {dispatch(actions.initTimeLineList())},
        getMyWriting: (params) => {dispatch(actions.getMyWriting(params))},
        initMyWriting: () => {dispatch(actions.initMyWritingList())},
        getFavoriteContent: (params) => {dispatch(actions.getFavoriteContent(params))},
        initFavoriteContent: () => {dispatch(actions.initFavoriteContentList())},
    };
};
export default connect(mapStateToProps, mapDispatchProps)(Community)
