import React, {Component} from 'react';
import {View, 
    Text, 
    Image, 
    StyleSheet, 
    TouchableHighlight, 
    Button,
    ScrollView} from 'react-native';
import {BaseComponent} from 'src-root/view/common/component/BaseComponent'
import CommunityCard from './CommunityCard'
import {CommonModal} from 'src-root/view/common/component/CommonModal'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions';

class InterestContent extends BaseComponent {

    constructor(props){
        super(props);
        this.state = {
            pageNum:1
        }

        this._selectedTab = this._selectedTab.bind(this);
    }

    _selectedTab = () => {
        console.log('interest Tab selected Tab');
    }

    componentWillMount(){
        this._init();
    }

    _init = () => {
        if(this.props.favoriteContentList.length > 0) 
            this.props.initFavoriteContent();

        params = {
            pageNum:1
        }
        this.props.getFavoriteContent(params);
    }

    _onListenFromCommunityCard = (listenType, params) => {
        switch(listenType){
            case 1:         // deleteContent
            {
                // this._reload();
                return;
            }
            case 2:
            {
                this.setState({contentInfo:params});
                this._showModal();
            }
        }
    }

    _onScrollEnd() {
        if (this.props.favoriteContentPageFetch) {
            this.setState({pageNum:++this.state.pageNum})
        }

        params = {
            pageNum:this.state.pageNum
        }
        this.props.getFavoriteContent(params);
    }
    
    _isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {

        if (layoutMeasurement.height + contentOffset.y >= contentSize.height-10) return true;
        else return false;
      };

    // _onPressButton(id) {
    //     this.props.navigation.navigate('DetailContent', {item: this.props.item});
    // }

    _getModal = () => {
        var btn = [
            {
                func : () => this._deleteFavoriteContent(),
                btnNm : "관심 삭제"
            }
        ];

        return (<CommonModal ref="modal" button = {btn}/>)
    }

    _deleteFavoriteContent = () => {
        const params = {
            postNo:this.state.contentInfo.postNo,
            memId:this.state.contentInfo.regId
        }
        var callback = () => this._init();

        this.props.delFavoriteContent(params, callback);
        this._dismissModal();
    }

    _showModal = () => {
        if (this.refs['modal'])
            this.refs['modal'].showModal();
    }

    _dismissModal = () => {
        if (this.refs['modal'])
            this.refs['modal'].dismissModal();
    }

    render() {
        return (
            <View style={{flex:1}}>
                {this.props.favoriteContentList.length === 0 ? 
                    <View style={styles.blankStyle}> 
                        <Text> 리스트가 없습니다. </Text>
                    </View>:
                    <View style={{flex:1, backgroundColor:'white'}}>
                        {this._getModal()}
                        <KeyboardAwareScrollView bounces={false} onMomentumScrollEnd={({nativeEvent}) => {
                                        if (this._isCloseToBottom(nativeEvent)) {
                                            this._onScrollEnd();
                                        }
                                    }}>
                            {this.props.favoriteContentList.map((item, idx) => {
                                return <CommunityCard
                                        callAction = {this._onListenFromCommunityCard.bind(this)} 
                                        item={item} key={idx} navigation={this.props.navigation}/>})
                            }
                        </KeyboardAwareScrollView>
                    </View>
                }
            </View>
        );
    }
}
const styles = StyleSheet.create({
    blankStyle:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: 'white'
    },
    contentContainer: {
        height: 140,
        backgroundColor: 'white',
        marginBottom: 5,
    },
    contentContainerTop: {
        height: 100, flexDirection: 'row', alignItems: 'center'
    },
    contentContainerBottom: {
        height: 50, flexDirection: 'row'
    },
    contentLeft: {
        width: 100
    },
    contentRight: {
        flex: 1, marginLeft: 15
    },
    contentBottom: {
        width: 100,
        marginLeft: 10
    },
    contentText: {
        color: 'gray', fontSize: 15
    },
    contentImg: {
        margin: 3, marginTop: 0, width: 100, height: 100
    }
});

const mapStateToProps = (state) => {
    
        return {
            favoriteContentList: state.community.favoriteContentList,
            favoriteContentPageFetch: state.community.favoriteContentPageFetch,
        };
    };
    
    const mapDispatchProps = (dispatch) => {
        return {
            getFavoriteContent: (params) => {dispatch(actions.getFavoriteContent(params))},
            initFavoriteContent: () => {dispatch(actions.initFavoriteContentList())},
            delFavoriteContent: (contentInfo, callback) => {dispatch(actions.delFavoriteContent(contentInfo, callback))},
        };
    };
    
    export default connect(mapStateToProps, mapDispatchProps)(InterestContent)