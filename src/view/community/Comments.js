import React from 'react'

import {
    View,
    Text,
    ScrollView,
    Image,
    Dimensions,
    TouchableOpacity,
    TextInput,
    Platform,
    Keyboard,
    KeyboardAvoidingView
} from 'react-native'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import {BaseComponent} from 'src-root/view/common/component/BaseComponent'
import {EvilIcons} from '@expo/vector-icons'
import {commonStyles} from 'src-root/style/CommonStyle'

import { connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions';

const BOTTOM_HEIGHT = 50
var comments = []

class Comments extends BaseComponent{
    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel : '댓글',
            title : '댓글',
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            keyboardSpace:0,
            nicknameModalVisible:true,
        };
        
        //for get keyboard height
        Keyboard.addListener('keyboardDidShow',(frames)=>{
            if (!frames.endCoordinates) return;
            this.setState({keyboardSpace: frames.endCoordinates.height});
        });
        Keyboard.addListener('keyboardDidHide',(frames)=>{
            this.setState({keyboardSpace:0});
        });
    }

    componentWillUnmount(){
        Keyboard.removeAllListeners('keyboardDidShow');
        Keyboard.removeAllListeners('keyboardDidHide');
    }

    _writeComment = () => {
        var params = {
            postNo :  this.props.navigation.state.params.postNo,
            cmmtCnte : this.state.writeComment
        }

        var callback = () => {
            this._getCommentList()
        };

        this.props.writeComment(params, callback);
    }

    _getCommentList = () => {
        var params = {
            postNo:this.props.navigation.state.params.postNo,
            pageNum:0
        }

        var callback = () => {
            this.props.navigation.goBack();
        }

        this.props.getComment(params, callback);
    }

    render(){
        comments = this.props.navigation.state.params.comments;

        return (
            <View
                style={{flex:1, backgroundColor:'white', paddingBottom:this.state.keyboardSpace}}>
                <ScrollView style={{flex:1}} bounces={false}>
                    { comments && comments.length > 0 ?
                        comments.map((item, idx) => {
                        return (
                            <View style = {{flexDirection:'row', width:'100%', margin:10}}  key={idx}>
                                <View style = {{width:'15%',
                                                // height:70,
                                                alignItems:'center',
                                                justifyContent:'center',}}>
                                    {item.userInfo && item.userInfo.pic1 ?                      
                                        <Image source={{uri:userInfo.pic1}}
                                                style = {commonStyles.roundImageSmall}
                                        />
                                        : <Image source={require('asset/profile_default_m.png')}
                                                style = {commonStyles.roundImageSmall}
                                        />
                                    }
                                </View>
                                <View style={{paddingLeft:5, paddingRight:5, width:'70%'}}>
                                    <Text style={{fontWeight:'bold'}}> {item.memNm} </Text>
                                    <Text style={{color:'rgb(100,100,100)'}}> {item.cmmtCnte} </Text>
                                </View>
                            </View>
                        )})
                    :<View/>}
                </ScrollView>

                <View style={{
                    position: 'absolute',
                    bottom: 0, 
                    flexDirection:"row", 
                    justifyContent:'center', 
                    alignItems:'center', 
                    height:BOTTOM_HEIGHT,
                    borderTopWidth:1,
                    backgroundColor:'white',
                    marginBottom:this.state.keyboardSpace,
                    // padding:10
                }}>
                    
                    <EvilIcons size = {30} name={'comment'}/>
                    <TextInput style={{
                            marginLeft:5, 
                            width:'80%', 
                            height:30,
                            borderBottomColor: 'rgba(111,210,233,0.7)',
                            borderBottomWidth:1,
                            paddingBottom:10}} 
                            autoCapitalize="none"
                        autoCorrect={false} 
                        underlineColorAndroid="transparent"
                        placeholder = {'댓글 달기...'} editable = {true}
                        onChangeText = {(text) => this.setState({writeComment: text})}
                        onFocus = {() => {}}/> 
                    <TouchableOpacity onPress = {this._writeComment} >
                        <Text> 게시 </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    
    return {
        
    };
};

const mapDispatchProps = (dispatch) => {
    return {
        getComment: (commentInfo, callback) => {dispatch(actions.getComment(commentInfo, callback))},
        writeComment: (commentInfo, callback) => {dispatch(actions.writeComment(commentInfo, callback))},
    };
};

export default connect(mapStateToProps, mapDispatchProps)(Comments)