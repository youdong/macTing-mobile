import React, {Component} from 'react';
import {View, 
    Text, 
    Image, 
    StyleSheet, 
    TouchableHighlight, 
    Button,
    TextInput,
    TouchableOpacity,
    ScrollView
} from 'react-native';

import {EvilIcons} from '@expo/vector-icons'
import {BaseComponent} from 'src-root/view/common/component/BaseComponent'
import {MaterialCommunityIcons} from '@expo/vector-icons'
import StringUtil from 'src-root/util/StringUtil'
import CommonFunc from 'src-root/view/common/component/CommonFunc'
import {commonStyles} from 'src-root/style/CommonStyle'

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions';

const AbbrLength = 30;
var cardData;

class CommunityCard extends BaseComponent {

    constructor(props) {
        super(props);
        this.state ={
            aspect:1,

            textContent : '',
            contentAbbreviate : false,
            commentAbbreviate : true,
            writeComment:'',
            commentCount:0,
            comments:[],

            showComment:false
        }
    }

    componentWillMount() {        
        this._init();
    }

    _init = () => {
        var contentInfo = this.props.item;
        
        // image size aspect 계산
        // var images = contentInfo.images;
        // if (images){
        //     if (images.length > 0 && images[0].fileNm){
        //         Image.getSize(images[0].fileNm, (width, height) => {
        //             this.setState({aspect:width/height});
        //        });
        //     }
        // }

        var postCnte = contentInfo.postCnte;
        if (postCnte){
            this.setState({textContent:postCnte});

            if (postCnte.length > AbbrLength)
                this.setState({ contentAbbreviate: true })
        }
    }

    

    _onPressContent = () => {
        if (this.state.textContent.length > AbbrLength)
            this.setState({ contentAbbreviate: !this.state.contentAbbreviate })
    }

    _onPressComment = () => {
        if (this.state.textContent.length > AbbrLength)
            this.setState({ commentAbbreviate: !this.state.commentAbbreviate})
    }

    _onPressOption = () => {

        var params = {
            postCnte:this.props.item.postCnte,
            postNo:this.props.item.postNo,
            postTit:this.props.item.postTit,
            regId:this.props.item.custNo,
            pic1:(this.props.item.images && this.props.item.images.length > 0 && this.props.item.images[0].fileNm 
                ? this.props.item.images[0].fileNm : "")
        };

        this.props.callAction(2, params);
    }

    _getImage = (cardData) => {
        if (cardData.images){
            if (cardData.images.length > 0 && cardData.images[0].fileNm){
                return (<Image source = {{uri: cardData.images[0].fileNm}}
                    style={{height:null, width:'100%', flex:1, aspectRatio: this.state.aspect}}
                    resizeMode='stretch'/>)
            }
        }
    }

    _onFocusComment = () => {

    }

    _getCommentCount = () => {
        if (this.props.item && this.props.item.comments)
            return (this.props.item.comments.length > 0 ? this.props.item.comments.length : "")
    }

    _getCommentHeight = () =>{
        return (this.state.showComment ? 200 : 0);
    }

    _onPressComment = () => {
        const params = {
            comments:this.props.item.comments,
            postNo:this.props.item.postNo
        };
        this.props.navigation.navigate('Comments', params);
        // this.setState({showComment:!this.state.showComment})
    }

    render() {
        cardData = this.props.item;

        return (
            <View>
                {/* title */}
                <View style = {styles.title}>
                    {/* image */}
                    <View style={{margin:5}}>
                        {(cardData && cardData.pic1 ? 
                        <Image 
                            // source={{uri: 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg'}}
                            source={{uri:cardData.pic1}}
                            style = {styles.roundImage}/>
                            :
                        <Image 
                            source={require('asset/profile_default_m.png')}
                            style = {styles.roundImage}/>)
                        }
                        
                    </View>
                    {/* basic information - 이름, 나이, 지역 */}
                    <View style={{margin:5}}>
                        <Text style={{fontSize:15, fontWeight:'bold'}}> {cardData.memNm} </Text>
                        <Text style={{fontSize:10, color:'#333333'}}> 1분전 </Text>
                    </View>
                    {/* button */}
                    <View style={{margin:5, flex:1, alignItems:'flex-end'}}>
                        <TouchableHighlight onPress = {this._onPressOption}>
                            <MaterialCommunityIcons size = {20} name={'dots-horizontal'}/>
                        </TouchableHighlight>
                    </View>
                </View>
                
                {/* contents */}
                <View>
                    {/* photo */}
                    <View style={{flexDirection: "row", flex: 1}}>
                        {this._getImage(cardData)}
                    </View>
                    
                    {/* text */}
                    <Text onPress={this._onPressContent}
                        style={{color:'#333333', 
                            marginTop:20, marginBottom:10, marginLeft:20, marginRight:20}}> 
                            {/* {cardData.postCnte} */}
                            {this.state.contentAbbreviate ? this.state.textContent.substr(0, AbbrLength) : this.state.textContent}
                            {this.state.contentAbbreviate ? <Text style={{color:'rgb(185,185,185)'}}> ... more </Text> : <Text/>}
                    </Text>

                    {/* tag */}
                    <Text style={{
                        fontWeight:'bold',
                        color:'rgb(94,194,189)',
                        marginBottom:10, marginLeft:20, marginRight:20}}>
                        {StringUtil.getTagExpFromArray(cardData.contentTags)}
                    </Text>
                </View>

                {/* comment */}
                <View>
                    <TouchableOpacity onPress = {()=> {this._onPressComment}} >
                        <Text onPress={this._onPressComment}
                            style ={{
                            marginLeft:20, marginRight:20, marginBottom:10,
                            fontSize:15,
                            fontWeight:'bold'}}> 
                            댓글 { this.props.item && this.props.item.comments && this.props.item.comments.length > 0 ?   
                            this._getCommentCount()+" 모두 보기" : " 쓰기"}
                            {/* 댓글 {this._getCommentCount()} {this.state.showComment ? "감추기" : "모두 보기..."} */}
                        </Text>
                    </TouchableOpacity>
                </View>

                {/* division */}
                <View style={{marginTop:10, height:3, width:'100%', backgroundColor:'rgb(185,185,185)'}}/>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    
    return {
        // regId: 
    };
};

const mapDispatchProps = (dispatch) => {
    return {
        
    };
};

export default connect(mapStateToProps, mapDispatchProps)(CommunityCard)

const styles = StyleSheet.create({
    title:{
        height : 54,
        flexDirection : 'row',
        width : '100%',
        // marginBottom:10
    },
    roundImage:{
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:44,
        height:44,
        backgroundColor:'#fff',
        borderRadius:22,
    },

});