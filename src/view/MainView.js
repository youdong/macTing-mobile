import React, {PropTypes} from 'react';
import {
    View, 
    ActivityIndicator, 
    StyleSheet,
    KeyboardAvoidingView
} from 'react-native';
import { connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import BaseStackNavigator from 'src-root/view/common/navigator/BaseStackNavigator'
import IntroStackNavigator from 'src-root/view/common/navigator/IntroStackNavigator'
import {BaseComponent} from 'src-root/view/common/component/BaseComponent'
import Login from 'src-root/view/intro/Login'
import UserRegist from 'src-root/view/intro/UserRegist'
import Intro from 'src-root/view/intro/Intro'

class MainView extends BaseComponent {
    constructor(props) {
        super(props);
    }

    _showIndicator() {
        if(this.props.isFetching)
            return(
                <View style = {style.loading}>
                    <ActivityIndicator size='large' />
                </View>
            )
        else
            return(
                <View/>
            )
    }

    render() {
        return (
             <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        {!this.props.isAppLoaded ? (<Intro/>) :                                 // intro 화면 
                            this.props.isLogin ? (<BaseStackNavigator/>) :                      // login 이후 앱 실행
                                (<IntroStackNavigator/>)}                                                 
                    </View>
                    {this._showIndicator()}
            </View>
        );
    }
}

const style = StyleSheet.create({
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
      }
})

const mapStateToProps = (state) => {
    return {
        isAppLoaded: state.intro.isAppLoaded,
        isLogin: state.login.isLogin,
        getIntoRegist: state.login.getIntoRegist,
        isFetching : state.network.isFetching
    };
};

export default connect(mapStateToProps)(MainView);
