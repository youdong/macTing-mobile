import React from 'react';
import { ScrollView, 
    View, 
    Text, 
    StyleSheet, 
    Image, 
    SafeAreaView,
    TouchableOpacity
 } from 'react-native'
import { BaseComponent } from 'src-root/view/common/component/BaseComponent'
import Dimensions from 'Dimensions'
import ContentRow  from '../lib/ContentRow'
import DivisionLine from '../lib/DivisionLine'

import {connect} from 'react-redux';
import * as actions from '../../actions';

import {commonColor} from 'src-root/style/CommonStyle'

import ViewPager from '../lib/viewPager/ViewPager'

var item = {};
var otherContents = [];

class Profile extends BaseComponent{
    static navigationOptions = {
        title : '상세보기'
    }

    constructor(props){
        super(props)
        item = this.props.navigation.state.params.item;
        otherContents = this.props.profileInfo.otherContents;

        var dataSource = new ViewPager.DataSource({
            pageHasChanged: (p1, p2) => p1 !== p2,
          });

        this.state = {
            dataSource: dataSource.cloneWithPages(this.props.profileInfo.pictures),
            showImageTicker : 0//this.props.profileInfo.showImageTicker
        }
        
    }

    componentWillMount(){

    }

    _renderPage(
        data,
        pageID) {
        return (
          <Image source={{uri: data}} style={style.image} >
            {/* <Text style={style.overlayText}>{item.memNm}</Text>
            <Text style={style.overlayText}>{item.age}</Text>
            <Text style={style.overlayText}>{item.ctNm}</Text> */}
        </Image>
        );
      }

    _showImageTicker() {
        switch (this.state.showImageTicker) {
            case 1:
                return(
                <View style={[style.imageTicker, {backgroundColor : commonColor.pink}]}>
                    <Text style = {style.tickerText}> 상대방에게 좋아요를 보냈습니다. </Text>
                </View>    
                )
                break;
            case 2:
                return(
                <View style={[style.imageTicker, {backgroundColor : commonColor.pink}]}>
                    <Text style = {style.tickerText}> 상대방이 나를 좋아합니다. </Text>
                </View>    
                )
                break;
            case 3:
                return(
                <View style={[style.imageTicker, {backgroundColor : commonColor.pink}]}>
                    <Text style = {style.tickerText}> 축하합니다! 상대방과 연결되었습니다. </Text>
                </View>    
                )
                break;
        
            default:
                return (
                <View style={[style.imageTicker, {backgroundColor : commonColor.mint}]}>
                    {/* <Text style = {style.tickerText}> 서비스 {item.isUser?'가입자':'미가입자'} 입니다. {item.countFriends > 0 ? <Text> 함께 아는 친구가 ({<Text style = {{color:commonColor.pink}}> {item.countFriends} </Text> })명 있습니다. </Text> : <Text> 함께 아는 친구가 없습니다.</Text>}</Text> */}
                </View> )
                break;
        }  
    }

    _onClickButtonLike = () =>{

    }

    _onClickButtonConnect = () => {

    }

    _onClickButtonInterest = () => {

    }

    _getButtons = () => {
        return (
            <View style={style.imageButton}>
                <TouchableOpacity style = {{margin:10}} onPress = {this._onClickButtonLike}>
                    <Image source={require('asset/profile_like.png')} /> 
                </TouchableOpacity>
                <TouchableOpacity style = {{margin:10}} onPress = {this._onClickButtonConnect}>
                    <Image source={require('asset/profile_connect.png')} />
                </TouchableOpacity>
                <TouchableOpacity style = {{margin:10}} onPress = {this._onClickButtonInterest}>
                    <Image source={require('asset/profile_interest.png')} />
                </TouchableOpacity>
            </View>
        )
    }

   render(){
       console.log("ttttt ", this.props);
       return(
           <SafeAreaView style={{flex:1, backgroundColor:'white'}}>
                <ScrollView style = {style.container} bounces={false}>
                    <View style = {style.imageView}>
                        <ViewPager
                            style={style.image}
                            dataSource={this.state.dataSource}
                            renderPage={this._renderPage}>
                        </ViewPager>

                        {/* <Text style={style.overlayText}>{item.memNm}</Text>
                        <Text style={style.overlayText}>{item.age}</Text>
                        <Text style={style.overlayText}>{item.ctNm}</Text> */}

                    </View>

                    { this._showImageTicker() }                    

                    {this._getButtons()}

                    <View style = {style.dataView}>
                        <View style={style.baseInfo}>
                            <Text style={style.overlayText}>{item.memNm}</Text>
                            <Text style={style.overlayText}>{item.age} | {item.ctNm}</Text>
                        </View>

                        <Text style={{
                                marginTop:15,
                                marginBottom:10,
                                fontSize:18,
                                fontWeight:'bold',
                                color:'rgb(150,150,150)'
                            }}>
                            <Text style={{color:commonColor.mint}}>상대</Text>가 직접 작성한 정보입니다.
                        </Text>

                        <View>
                            <ContentRow title={'직업'} key={item.idx+'0'} content={item.jobNm}/>
                            <ContentRow title={'직장'} key={item.idx+'1'} content={item.cmpNm}/>
                            <ContentRow title={'종교'} key={item.idx+'2'} content={item.rlgnNm}/>
                        </View>
                        
                        <Text style={{
                                marginTop:15,
                                marginBottom:10,
                                fontSize:18,
                                fontWeight:'bold',
                                color:'rgb(150,150,150)'
                            }}>
                            <Text style={{color:commonColor.mint}}>{item.countFriends}명의 친구</Text>가 작성한 정보입니다.
                        </Text>

                        <View>
                        {
                            otherContents.map((other, idx) => {
                                return  (
                                    <View key={idx}>
                                        {idx === 0 ? null :<DivisionLine margin={10} color={'gray'} />}
                                        <ContentRow title={'체형'} key={idx+'3'} content={other.bodyType}/>
                                        <ContentRow title={'스타일'} key={idx+'4'} content={other.style}/>
                                        <ContentRow title={'성격'} key={idx+'5'} content={other.character}/>
                                    </View>
                                )
                            })
                        }
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
       );
   } 
}
const win = Dimensions.get('window');
const style = StyleSheet.create({
    container: {
      flex:2,
      marginTop:10  
    },
    imageView: {
        flex : 1,
        width: win.width,
        height: win.height/2,
    },
    image: {
        flex: 1,
        width: win.width,
        height: win.height/2,
        // resizeMode: 'cover',
        
    },
    imageTicker:{
        width: win.width,
        opacity: 0.8,
        alignItems : "center",
        justifyContent : "center",
    },
    tickerText:{
        color: 'white', 
        fontWeight: 'bold',
        padding:5
    },
    overlayText: {
        margin: 3,
        fontSize: 22,
        // width: win.width,
        // alignItems: 'flex-end',
        fontWeight: 'bold',
        // color: 'Black',
        // backgroundColor: 'transparent',
      },
    dataView: {
        flex : 1,
        justifyContent: `flex-start`,
        backgroundColor: 'white',
        alignSelf: 'stretch',
        margin:30
    },
    friendInfo: {
        justifyContent: `flex-start`,
        alignItems: `flex-start`,
        margin: 5
    },
    imageButton: {
        flexDirection:'row',
        alignItems: `center`,
        justifyContent: `center`,
        backgroundColor:'white',
        borderBottomWidth:1,
        borderBottomColor:'rgba(100,100,100,0.5)',
        margin: 10,
        paddingBottom: 10
    },
    profileContent: {
        alignItems: `flex-start`
    },
    baseInfo:{
        marginBottom:10
    },
    page: {
        width: Dimensions.get('window').width,
      }
});

const mapStateToProps = (state) => {
    
        return {
            profileInfo: state.profile.profileInfo
        };
    };
    
    const mapDispatchProps = (dispatch) => {
        return {
            
        };
    };
    
    export default connect(mapStateToProps, mapDispatchProps)(Profile)