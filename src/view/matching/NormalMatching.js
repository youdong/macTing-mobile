import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, TouchableHighlight, ScrollView, TouchableOpacity} from 'react-native';
import {BaseComponent} from 'src-root/view/common/component/BaseComponent'
import MatchingList from './MatchingListView'

import {commonColor} from 'src-root/style/CommonStyle'

import {connect} from 'react-redux';
import * as actions from '../../actions';

var NormalMatchingInfo;
class NormalMatching extends BaseComponent{

    static navigationOptions = {
        tabBarLabel : '오늘의 소개',
        title : '오늘의 소개'
    }

    componentWillMount(){
        // this.props.getProfileInfo({memId:'udong'});

        NormalMatchingInfo = this.props.matchingList;
    }

    render(){
        return(
            <View style={style.mainView}>
                <ScrollView bounces={false}>
                    {/* 지나간 맥팅 (리스트) */}
                    {/* <View style={{margin: 10}}>
                            <Text style={{
                                color:'rgb(185,185,185)',
                                fontWeight:'bold',
                                fontSize:20}}> 
                                지나간 <Text style={{color:'rgb(248,122,141)'}}>맥팅</Text>
                            </Text>
                        </View> */}
                        <View style={{
                                    alignItems:'center',
                                    justifyContent:'center'}}>
                            {NormalMatchingInfo && NormalMatchingInfo.length > 0 ?
                                NormalMatchingInfo.map((item, idx) => 
                                    <MatchingList style={{}} item={item} key={idx} idx={idx} navigation={this.props.navigation} isConnected = {true}/>)
                                :<View/>
                            }
                        </View>
                </ScrollView>

                <View style={{
                            justifyContent:'center',
                            alignItems:'center',
                            // marginTop:25,
                            marginBottom:20
                        }}>
                            <TouchableOpacity onPress = {() => {}}>
                                <View style = {style.buttonStyle}>
                                    <Text style={style.buttonTextStyle}> 일반 매칭 카드 다시 받기 </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
            </View>
        );  
    }
}

const style = StyleSheet.create({
    mainView : {
        flex: 1,
        backgroundColor:'white'
        // flexDirection: `column`,
        // alignItems: `center`,
        // justifyContent: `center`
    },
    buttonStyle:{
        justifyContent:'center',
        alignItems:'center',
        // borderWidth:1,
        borderRadius:15,
        padding:15,
        backgroundColor:commonColor.pink
    },
    buttonTextStyle:{
        color:'rgb(245,245,245)',
        fontSize:16,
        fontWeight:'bold'
    },
});

const mapStateToProps = (state) => {
    
        return {
            matchingList: state.matching.matchingList
        };
    };
    
    const mapDispatchProps = (dispatch) => {
        return {
            getProfileInfo: (id) => {dispatch(actions.getProfileInfo(id))},
            searchMatchingInfo: () => {dispatch(actions.searchMatchInfo())},
        };
    };
    
    export default connect(mapStateToProps, mapDispatchProps)(NormalMatching)