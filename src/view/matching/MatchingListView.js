import React, { Component } from 'react';
import { Alert, 
    View, 
    Text,
    Image,
    StyleSheet,
    TouchableHighlight, 
    Dimensions 
} from 'react-native';

import {BaseComponent} from 'src-root/view/common/component/BaseComponent'

const TextMargin = 15;

var cardViewWidth = 100;
var cardViewHeight = 200;

export default class CardView extends BaseComponent {

    componentWillMount() {
        cardViewWidth = Dimensions.get('window').width * 90/ 100
    }

    _onPressButton(id) {
        this.props.navigation.navigate('Profile', { item: this.props.item, key:id});
    }

    _getCardView = (_width, _height) => {
        return(
            <View style={{backgroundColor:"white",
                        borderColor:'rgb(185,185,185)',
                        // borderWidth:1,
                        borderBottomWidth:1,
                        // borderRadius:15,
                        width:_width,
                        flexDirection:'row'
                        // height:_height
                        }}>
                {/* photo */}
                <View style = {{ margin:20,
                    // width:cardViewWidth, height:cardViewWidth * 2 / 3,
                                alignItems:'center',
                                justifyContent:'center',}}>
                        {this.props.item.pic1 ? 
                            <Image source={{uri:this.props.item.pic1}}
                                    style = {style.roundImage}/>
                            :<Image source={require('asset/profile_default_m.png')}
                                style = {style.roundImage}/>
                        }
                </View>

                <View>
                    {/* base Info */}
                    <View style={{margin:TextMargin}}>
                        <Text style={{fontSize:15, fontWeight:'bold'}}>
                            {this.props.item.memNm} / {this.props.item.ctNm} / {this.props.item.age}
                        </Text>
                        <Text style={{fontWeight:'bold',
                            marginTop:5,
                            color:'rgb(94,194,189)'}}>
                            {this.props.item.tag}
                        </Text>
                    </View>

                    {/* connected info*/}
                    <View style={{margin:TextMargin}}>
                        <Text style={{
                            color:'rgb(185,185,185)',
                            fontWeight:'bold',
                            fontSize:10}}>
                            서비스 {this.props.item.isUser?'가입자':'미가입자'} 입니다.
                        </Text>
                        <Text style={{
                            color:'rgb(185,185,185)',
                            fontWeight:'bold',
                            fontSize:10}}>
                            함께 아는 친구가 <Text style={{color:'rgb(248,122,141)'}}>({this.props.item.countFriends})</Text> 명 있습니다.
                        </Text>
                    </View>
                </View>
            </View>
        )
    }

    render(){
        return(
            <View style={{margin:5}}>
                <TouchableHighlight onPress={this._onPressButton.bind(this, this.props.idx)} underlayColor="white" >
                    {this._getCardView(cardViewWidth, cardViewHeight)}
                </TouchableHighlight>
            </View>
        );  
    }
}

const style = StyleSheet.create({
    imageView:{
        width:'30%',
        height:70,
        alignItems:'center',
        justifyContent:'center',
      },
    roundImage:{
        borderWidth:0,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:66,
        height:66,
        backgroundColor:'#fff',
        borderRadius:33,
    },




    // container: {
    //     flex: 1,
    //     flexDirection: `column`
    // },
    // cardView: {
    //     flex: 1,
    //     flexDirection: `column`,
    //     // borderRadius:50,
    //     backgroundColor:'#fffee7',
    //     // alignItems: `center`,
    //     margin: 10
    // },
    // touchableHighlight: {
    //     flex: 1,
    //     flexDirection: `column`,
    //     borderRadius:50
    // },
    // title: {
    //     flex: 1,
    //     height: 50
    // },
    // content: {
    //     flex: 2,
    //     flexDirection: `row`,
    // },
    // bottom: {
    //     flex: 1,
    //     margin: 10,
    //     // height: 80,
    //     justifyContent: `flex-start`
    // },
    // image: {
    //     flex: 1,
    //     margin : 10,
    //     width : photoSlotSize,
    //     height : photoSlotSize
    // },
    // detail: {
    //     flex: 2,
    //     margin: 10
    // }
});

