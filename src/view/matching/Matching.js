import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, TouchableHighlight, ScrollView} from 'react-native';
import {BaseComponent} from 'src-root/view/common/component/BaseComponent'

import {connect} from 'react-redux';
import * as actions from '../../actions';

import TabView from 'src-root/view/common/component/TabView'
import TabItem from 'src-root/view/common/component/TabItem'

import TodayMatching from './TodayMatching'
import NormalMatching from './NormalMatching'

class Matching extends BaseComponent{

    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel : '인연찾기',
            title : '인연찾기',
            tabBarOnPress: (scene, jumpToIndex) => {
                if (!scene.focused){
                    jumpToIndex(scene.index);
                    
                    const navigationInRoute = scene.route;
                    if(!!navigationInRoute && !!navigationInRoute.params && !!navigationInRoute.params.selectedTab)
                        navigation.state.params.selectedTab();
                }
            },
        }
    }

    _selectedTab = () => {
        //Code to make ajax call
        this.refs['child']._selectedTab(0);
    }

    _onSelectTab = (index) => {        
        switch(index){
            case 0:
                // this.props.getIntroduceToday();
                break;
            case 1:
                
                break;
        }
    }
      
    componentDidMount(){
        this.props.navigation.setParams({selectedTab: this._selectedTab.bind(this)});

        this.props.getIntroduceToday();

        // 시간차를 주어서 세팅... 딱히 맘에 들지는 않는다 ㅠㅠㅠ
        // setTimeout(() => {
        //     console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
        //     this.props.navigation.setParams({selectedTab: this._selectedTab.bind(this)});
        //   }, 1000)
    }

    render(){
       const cardItem = this.props.matchingList;
        
        return(
            <View style={style.mainView}>
                <TabView ref="child" onSelectTab = {this._onSelectTab}>
                    <TabItem title="오늘의 매칭">
                        <TodayMatching navigation={this.props.navigation} todayMatchingList={this.props.todayMatchingList}/>
                    </TabItem>
                    <TabItem title="일반 매칭">
                        <NormalMatching navigation={this.props.navigation}/>
                    </TabItem>
                </TabView>
            </View>
        );  
    }
}

const style = StyleSheet.create({
    mainView : {
        flex: 1,
        flexDirection: `column`,
        // alignItems: `center`,
        // justifyContent: `center`
    }
});

const mapStateToProps = (state) => {
    
        return {
            todayMatchingList: state.matching.todayMatchingList
        };
    };
    
    const mapDispatchProps = (dispatch) => {
        return {
            getIntroduceToday:() => {dispatch(actions.getIntroduceToday())},
            searchMatchingInfo: () => {dispatch(actions.searchMatchInfo())},
        };
    };
    
    export default connect(mapStateToProps, mapDispatchProps)(Matching)