import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, TouchableHighlight, ScrollView} from 'react-native';
import {BaseComponent} from 'src-root/view/common/component/BaseComponent'
import MatchingCard from './MatchingCardView'
import MatchingList from './MatchingListView'

import {connect} from 'react-redux';
import * as actions from '../../actions';

var TodayMatchingInfo;
class TodayMatching extends BaseComponent{

    static navigationOptions = {
        tabBarLabel : '오늘의 소개',
        title : '오늘의 소개'
    }

    componentWillMount(){
        // this.props.getIntroduceToday();
    }

    render(){
        TodayMatchingInfo = this.props.todayMatchingList;
        
        return(
            <View style={{flex:1, backgroundColor:'white'}}>
                <ScrollView style={style.mainView} bounces={false}>
                    {/* 오늘의 맥팅 (카드) */}
                    <View style={{flexDirection : 'row',
                                alignItems:'center',
                                justifyContent:'center'}}>
                        {TodayMatchingInfo && TodayMatchingInfo.length > 0 ?
                            TodayMatchingInfo.map((item, idx) => 
                                <MatchingCard style={{}} item={item} key={idx} idx={idx} navigation={this.props.navigation} isConnected = {true}/>)
                            : <View/>
                        }
                    </View>
                    
                    {/* 지나간 맥팅 (리스트) */}
                    {TodayMatchingInfo && TodayMatchingInfo.length > 0 ?
                        <View>
                            <View style={{margin: 10}}>
                                <Text style={{
                                    color:'rgb(185,185,185)',
                                    fontWeight:'bold',
                                    fontSize:20}}> 
                                    지나간 <Text style={{color:'rgb(248,122,141)'}}>맥팅</Text>
                                </Text>
                            </View>
                            <View style={{
                                        alignItems:'center',
                                        justifyContent:'center'}}>
                                {TodayMatchingInfo && TodayMatchingInfo.length > 0 ?
                                    TodayMatchingInfo.map((item, idx) => 
                                        <MatchingList style={{}} item={item} key={idx} idx={idx} navigation={this.props.navigation} isConnected = {true}/>)
                                    : <View/>
                                }
                            </View>
                        </View>
                        :
                        <View/>
                    }
                </ScrollView>   
            </View>
        );  
    }
}

const style = StyleSheet.create({
    mainView : {
        flex: 1,
        flexDirection: `column`,
        // alignItems: `center`,
        // justifyContent: `center`
    }
});

const mapStateToProps = (state) => {
    
        return {
            // todayMatchingList: state.matching.todayMatchingList
        };
    };
    
    const mapDispatchProps = (dispatch) => {
        return {
            getIntroduceToday:() => {dispatch(actions.getIntroduceToday())},
            searchMatchingInfo: () => {dispatch(actions.searchMatchInfo())},
        };
    };
    
    export default connect(mapStateToProps, mapDispatchProps)(TodayMatching)