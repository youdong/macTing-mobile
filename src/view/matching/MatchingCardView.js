import React, { Component } from 'react';
import { Alert, 
    View, 
    Text,
    Image,
    StyleSheet,
    TouchableHighlight, 
    Dimensions 
} from 'react-native';

import StringUtil from 'src-root/util/StringUtil'
import {BaseComponent} from 'src-root/view/common/component/BaseComponent';

const TextMargin = 10;
const cardViewMargin = 5;

var cardViewWidth = 100;
var cardViewHeight = 200;

export default class CardView extends BaseComponent {

    componentWillMount() {
        cardViewWidth = Dimensions.get('window').width * 50/ 100
    }

    _onPressButton(id) {
        this.props.navigation.navigate('Profile', { item: this.props.item, key:id});
    }

    _getTag = (tags) => {
        return (
            <View style={{flexDirection:'row'}}>
                {tags.map((tag, idx) => 
                    <Text style={{
                        fontWeight:'bold',
                        margin:cardViewMargin,
                        color:'rgb(244,244,244)',
                        backgroundColor:'rgb(141,197,218)',
                        // borderWidth:1,
                        borderRadius:15,
                    }}>
                        #{tag}
                    </Text>
                )}
            </View>
        )
        tags
    }

    _getCardView = (_width, _height) => {
        return(
            <View style={{backgroundColor:"white",
                        borderColor:'rgb(237,237,237)',
                        borderWidth:5,
                        // borderRadius:15,
                        width:_width,
                        // height:_height
                        }}>
                {/* photo */}
                <View style = {{ margin:20,
                    // width:cardViewWidth, height:cardViewWidth * 2 / 3,
                                alignItems:'center',
                                justifyContent:'center',}}>
                        {this.props.item.pic1 ? 
                            <Image source={{uri:this.props.item.pic1}}
                                    style = {style.roundImage}/>
                            :<Image source={require('asset/profile_default_m.png')}
                                style = {style.roundImage}/>
                        }
                </View>
                {/* base Info */}
                <View style={{margin:TextMargin}}>
                    <Text style={{fontSize:15, fontWeight:'bold'}}>
                        {this.props.item.memNm} / {this.props.item.ctNm} / {this.props.item.age}
                    </Text>
                    {/* <Text style={{fontWeight:'bold',
                        marginTop:5,
                        color:'rgb(94,194,189)',
                        // backgroundColor:'rgb(141,197,218)',
                        // borderRadius:15,
                        }}>
                        {this.props.item.tag}
                    </Text> */}
                    {/* {this._getTag(this.props.item.tags)} */}
                    <Text style={{fontWeight:'bold',
                        marginTop:5,
                        color:'rgb(94,194,189)',
                        // backgroundColor:'rgb(141,197,218)',
                        // borderRadius:15,
                        }} 
                        numberOfLines = {1}>
                        {StringUtil.getTagExpFromArray2(this.props.item.tags)}
                    </Text>
                </View>

                {/* division */}
                <View style={{height:1, width:_width - (2*TextMargin) - (2*cardViewMargin), 
                            marginLeft:TextMargin, marginRight:TextMargin,
                            backgroundColor:'rgb(185,185,185)'}}/>

                {/* connected info*/}
                <View style={{margin:TextMargin}}>
                    <Text style={{
                        color:'rgb(185,185,185)',
                        fontWeight:'bold',
                        fontSize:10}}>
                        서비스 {this.props.item.isUser?'가입자':'미가입자'} 입니다.
                    </Text>
                    <Text style={{
                        color:'rgb(185,185,185)',
                        fontWeight:'bold',
                        fontSize:10}}>
                        함께 아는 친구가 <Text style={{color:'rgb(248,122,141)'}}>({this.props.item.countFriends})</Text> 명 있습니다.
                    </Text>
                </View>
            </View>
        )
    }

    render(){
        return(
            <View style={{margin:0}}>
                <TouchableHighlight onPress={this._onPressButton.bind(this, this.props.idx)} underlayColor="white" >
                    {this._getCardView(cardViewWidth, cardViewHeight)}
                </TouchableHighlight>
            </View>
            
            
            // <View style={style.container}>
            // <TouchableHighlight onPress={this._onPressButton.bind(this, this.props.idx)} underlayColor="white" >
            //     <View style={style.cardView}>
                    
            //         <View style={style.content}>
            //             <View>
            //                 <Image source={{uri: this.props.item.uri}} style={style.image} />
            //             </View>
            //             <View style={style.detail}>
            //                 <Text>{this.props.item.memNm }</Text>
            //                 <Text>{this.props.item.age} / {this.props.item.ctNm} / {this.props.item.jobNm}</Text>
            //                 <Text>{this.props.item.tag}</Text>
            //             </View>
            //         </View>
            //         {
            //             this.props.isConnected ?
            //             <View style={style.bottom}>
            //                 <Text style={{justifyContent: `flex-start`}}>서비스 {this.props.item.isUser?'가입자':'미가입자'} 입니다.</Text>
            //                 <Text>함께 아는 친구가 ({this.props.item.countFriends})명 있습니다.</Text>
            //             </View> : <View/>
            //         }
            //     </View>
            // </TouchableHighlight>
            // </View>
        );  
    }
}

const style = StyleSheet.create({
    imageView:{
        width:'30%',
        height:70,
        alignItems:'center',
        justifyContent:'center',
      },
    roundImage:{
        // borderWidth:1,
        // borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:120,
        height:120,
        backgroundColor:'#fff',
        borderRadius:60,
    },




    // container: {
    //     flex: 1,
    //     flexDirection: `column`
    // },
    // cardView: {
    //     flex: 1,
    //     flexDirection: `column`,
    //     // borderRadius:50,
    //     backgroundColor:'#fffee7',
    //     // alignItems: `center`,
    //     margin: 10
    // },
    // touchableHighlight: {
    //     flex: 1,
    //     flexDirection: `column`,
    //     borderRadius:50
    // },
    // title: {
    //     flex: 1,
    //     height: 50
    // },
    // content: {
    //     flex: 2,
    //     flexDirection: `row`,
    // },
    // bottom: {
    //     flex: 1,
    //     margin: 10,
    //     // height: 80,
    //     justifyContent: `flex-start`
    // },
    // image: {
    //     flex: 1,
    //     margin : 10,
    //     width : photoSlotSize,
    //     height : photoSlotSize
    // },
    // detail: {
    //     flex: 2,
    //     margin: 10
    // }
});

