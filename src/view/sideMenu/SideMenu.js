import {View, 
        Text, 
        StyleSheet, 
        ScrollView,
        TouchableOpacity,
        Image,
        AsyncStorage
      } from 'react-native';
import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Entypo} from '@expo/vector-icons'

import StringUtil from 'src-root/util/StringUtil'

import * as storageKey from 'src-root/util/AsyncStorageKey'

import * as actions from '../../actions';

class SideMenu extends React.Component{
  _close = () => {
    this.props.navigation.navigate('DrawerClose')
  }

  _moveScreen = (screen) => {
    this.props.navigation.navigate(screen)
  }

  _doLogoutBefore = async () => {

    try {
      await AsyncStorage.setItem(storageKey.LOGIN_PREFIX + storageKey.PASSWORD, '', () => { AsyncStorage.flushGetRequests()});

      this.props.doLogout();
    } catch (error) {
      // Error saving data
    }
  }

        render () {
            const {
              myProfile,
              navigation
            } = this.props;
            return (
              <View style={{flex:1}}>
                <View style = {style.header}>
                    <View style = {style.imageView}>
                      {myProfile && myProfile.pic1 ?                      
                        <Image source={{uri:myProfile.pic1}}
                                style = {style.roundImage}
                        />
                        : <Image source={require('asset/profile_default_m.png')}
                                style = {style.roundImage}
                          />
                      }
                    </View>
                    <View style={{paddingLeft:5, paddingRight:5, width:'70%'}}>
                      <View style={{flex:1, alignItems:'flex-end'}}>
                        <TouchableOpacity onPress = {this._close} >
                          <Entypo size = {30} name={'cross'}/>
                        </TouchableOpacity>
                      </View>
                      <View style = {{flex:1}}>
                        <Text> {myProfile.memNm} </Text>
                      </View>
                      <View style = {{flex:1}}>
                        <Text> {StringUtil.getPhoneNumWithFormat(myProfile.cpNo)} </Text>
                      </View>
                    </View>
                </View>
                <ScrollView bounces={false}>
                  <View>
                    <View style={style.sideMenuCell}>
                      {myProfile.pic1 ? 
                        <Text style={style.menuTitle}  onPress={() => this._moveScreen('MyProfile')}>
                          내 프로필 수정
                        </Text> :
                        <Text style={style.menuTitle}  onPress={() => this._moveScreen('MyProfile')}>
                          내 프로필 등록
                        </Text>
                      }                    
                    </View>
                    <View style={style.sideMenuCell}>
                      <Text style={style.menuTitle}>
                        포인트 조회 / 충전
                      </Text>                    
                    </View>
                    <View style={style.sideMenuCell}>
                      <Text style={style.menuTitle}>
                        알림 설정
                      </Text>                    
                    </View>
                    <View style={style.sideMenuCell}>
                      <Text style={style.menuTitle}>
                        계정 설정
                      </Text>                    
                    </View>
                    <View style={style.sideMenuCell}>
                                          
                    </View>
                    <View style={style.sideMenuCell}>
                      <Text style={style.menuTitle}>
                        서비스 안내
                      </Text>                    
                    </View>
                    <View style={style.sideMenuCell}>
                      <Text style={style.menuTitle}>
                        공지 사항
                      </Text>                    
                    </View>
                    <View style={style.sideMenuCell}>
                      <Text style={style.menuTitle}>
                        이벤트
                      </Text>                    
                    </View>
                    <View style={style.sideMenuCell}>
                      <Text style={style.menuTitle}>
                        고객 센터
                      </Text>                    
                    </View>
                    <View style={style.sideMenuCell}>
                      <Text style={style.menuTitle}>
                        
                      </Text>                    
                    </View>
                    <View style={style.sideMenuCell}>
                      <Text style={style.menuTitle} onPress={this._doLogoutBefore}>
                        로그아웃
                      </Text>                    
                    </View>
                  </View>
                </ScrollView>
              </View>
            )
        };
}

const style = StyleSheet.create({
    sideMenuCell:{
        flex:1,
        height:30,
        marginLeft: 10,
        marginTop:5,
        marginBottom:5,
        justifyContent :'center'
    },
    menuTitle:{
        fontSize:20
    },
    imageView:{
      width:'30%',
      height:70,
      alignItems:'center',
      justifyContent:'center',
    },
    roundImage:{
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:66,
        height:66,
        backgroundColor:'#fff',
        borderRadius:33,
    },
    header:{
        height : 70,
        flexDirection : 'row',
        width : '100%',
        marginBottom:10
    }
})

const mapStateToProps = (state) => {
      return {
          isLogin: state.login.isLogin,
          myProfile : state.profile.myProfile
          // userInfo : state.userInfo.userInfo
      };
  };
  
  const mapDispatchProps = (dispatch) => {
      return {
          doLogout: () => {dispatch(actions.doLogout())},
      };
  };
  
  export default connect(mapStateToProps, mapDispatchProps)(SideMenu)