import {BaseComponent} from 'src-root/view/common/component/BaseComponent'
import React, {Component} from 'react'
import {View, 
    Text, 
    StyleSheet, 
    ScrollView, 
    Dimensions, 
    Image,
    TouchableOpacity,
    Alert,
    TextInput,
    Keyboard
} from 'react-native'

import HeaderLeft from 'src-root/view/common/navigator/NavigatorHeaderLeft';
import HeaderRight from 'src-root/view/common/navigator/NavigatorHeaderRight';
import {commonColor} from 'src-root/style/CommonStyle'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {connect} from 'react-redux';
import * as actions from '../../../actions';
import ImageUtil from 'src-root/util/ImageUtil'
import StringUtil from 'src-root/util/StringUtil'

const photoSlotSize = Dimensions.get('window').width / 3 - 15

class MyProfile extends BaseComponent{

    static navigationOptions = ({navigation}) => {
        return {
            // tabBarLabel : '나의 프로필 보기',
            title : '나의 프로필 보기',

            headerRight: <HeaderRight 
                    height={19}
                    // icon={"list"}
                    text = {"저장"}
                    action={
                        () => {navigation.state.params.saveProfile()}
                    }
                />
        }
    }
    
    constructor(props) {
        super(props);
        this.state = {
            keyboardSpace:0,
            nicknameModalVisible:true,
        };
        
        //for get keyboard height
        Keyboard.addListener('keyboardDidShow',(frames)=>{
            if (!frames.endCoordinates) return;
            this.setState({keyboardSpace: frames.endCoordinates.height});
        });
        Keyboard.addListener('keyboardDidHide',(frames)=>{
            this.setState({keyboardSpace:0});
        });
    }

    componentWillUnmount(){
        Keyboard.removeAllListeners('keyboardDidShow');
        Keyboard.removeAllListeners('keyboardDidHide');
    }

    componentWillMount() {
        this.props.setMyProfileTemp();
    }

    componentDidMount() {
        this.props.navigation.setParams({ saveProfile: this._saveProfile });
        // this.props.navigation.setParams({ goBack: this._goBack });
    }

    _saveProfile = () => {
        params = {
            ctCd:this.state.ctCd,
            cmp:this.state.cmp,
            job:this.state.job,
            rlgnCd:this.state.rlgnCd,
            pic1:this.state.pic1,
            pic2:"",
            pic3:"",
        }

        images = StringUtil.makePicArray(this.state.pic1, this.state.pic2, this.state.pic3);

        callback = ()=>{this.props.getUserProfileInfo({memId:this.props.userId})};

        if (this.props.myProfile && this.props.myProfile.pic1){
            // profile modify
            this.props.modifyProfile(params, images, callback);
        }else{
            // profile regist
            this.props.registProfile(params, images, callback);
        }
    }

    // _goBack = () => {
    //     this.props.navigation.goBack();
    // }

    _setPhotoImage = (num) => {
        Alert.alert(
            'Alert Title',
            '_setPhotoImage',
            [
              {text: 'Cancel'},
              {text: 'OK', onPress: () => ImageUtil.pickImageFromGalary( (num, uri) => {this.props.addUserPhotoImage (num, uri)}, num)},
            ],
            { cancelable: false }
        )
    }

    _deletePhotoImage = (num) => {
        Alert.alert(
            'Alert Title',
            '_deletePhotoImage',
            [
              {text: 'Cancel'},
              {text: 'OK', onPress: () => this.props.deletePhotoImage(num)},
            ],
            { cancelable: false }
          )
    }

    _getImageIndex = (isMainPic) => {
        if (isMainPic) return 1;
        else {
            if (!this.props.myProfileTemp.pic2 || this.props.myProfileTemp.pic2 === "") return 2;
            else if (!this.props.myProfileTemp.pic3 || this.props.myProfileTemp.pic3 === "") return 3;
        }
    }

    _getPhotoEvent = (num) => {
        event = null
        myProfileTemp = this.props.myProfileTemp
        // this.setState(selectedPhoto)

        switch(num){
            case 0:
                return (myProfileTemp.pic1 ? () => this._deletePhotoImage(1) : () => this._setPhotoImage(this._getImageIndex(true)));
            case 1:
                return (myProfileTemp.pic2 ? () => this._deletePhotoImage(2) : () => this._setPhotoImage(this._getImageIndex(false)));
            case 2:
                return (myProfileTemp.pic3 ? () => this._deletePhotoImage(3) : () => this._setPhotoImage(this._getImageIndex(false)));
        }

        // if (userInfo.pictures[num]) event = () => this._deletePhotoImage(num)
        // else if (num === 0) event = this._setPhotoImage
        // else if (userInfo.pictures[num-1]) event = this._setPhotoImage
        // else event = null

        // return event
    }

   render(){
    myProfileTemp = this.props.myProfileTemp;

       return(
           <View style = {{flex : 1, backgroundColor:'white'}}>
                <KeyboardAwareScrollView bounces={false} style = {{marginBottom:this.state.keyboardSpace}}>
                    {/* 이미지 사진 */}
                    <View style={style.layoutTitle}>
                        <Text style={style.textTitleMain}> 사진 </Text> 
                        <Text style={style.textTitleSub}> 최소 2장 필수 </Text>
                    </View>
                    <View style={style.photoView}>
                        <TouchableOpacity onPress = {this._getPhotoEvent(0)} style = {style.photo}>
                            <View style={[{width:photoSlotSize, height:photoSlotSize + 10, alignItems:'center',}]}>
                                {myProfileTemp.pic1 
                                ?<Image source={{uri:myProfileTemp.pic1}} style = {{width:photoSlotSize, height:photoSlotSize, borderRadius:15}}/>
                                :
                                
                                    <View style={[{width:photoSlotSize, height:photoSlotSize, borderColor:commonColor.pink}, style.photoBorder]}>
                                        <Image source={require('asset/plus.png')} style = {{width:photoSlotSize/2, height:photoSlotSize/2}}/>
                                    </View>
                                    
                                }
                                <Text style={{color:commonColor.pink, fontWeight:'bold'}}>대표 이미지</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress = {this._getPhotoEvent(1)} style = {style.photo}>
                            <View style={[{width:photoSlotSize, height:photoSlotSize + 10, alignItems:'center',}]}>
                            {myProfileTemp.pic2 
                                ?<Image source={{uri:myProfileTemp.pic2}} style = {{width:photoSlotSize, height:photoSlotSize, borderRadius:15}}/>
                                :
                                    <View style={[{width:photoSlotSize, height:photoSlotSize, borderColor:'rgb(200,200,200)'}, style.photoBorder]}>
                                        <Image source={require('asset/plus.png')} style = {{width:photoSlotSize/2, height:photoSlotSize/2}}/>
                                    </View>
                                }
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress = {this._getPhotoEvent(2)} style = {style.photo}>
                            <View style={[{width:photoSlotSize, height:photoSlotSize + 10, alignItems:'center',}]}>
                                {myProfileTemp.pic3 
                                ?<Image source={{uri:myProfileTemp.pic3}} style = {{width:photoSlotSize, height:photoSlotSize, borderRadius:15}}/>
                                :
                                    <View style={[{width:photoSlotSize, height:photoSlotSize, borderColor:'rgb(200,200,200)'}, style.photoBorder]}>
                                        <Image source={require('asset/plus.png')} style = {{width:photoSlotSize/2, height:photoSlotSize/2}}/>
                                    </View>
                                }
                            </View>
                        </TouchableOpacity>
                    </View>
                    {/* <View style={style.photoView}>
                        <TouchableOpacity onPress = {this._getPhotoEvent(3)} style = {style.photo}>
                            <Image source={{uri:this._getPhotoImage(3)}} style = {{width:photoSlotSize, height:photoSlotSize}}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress = {this._getPhotoEvent(4)} style = {style.photo}>
                            <Image source={{uri:this._getPhotoImage(4)}} style = {{width:photoSlotSize, height:photoSlotSize}}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress = {this._getPhotoEvent(5)} style = {style.photo}>
                            <Image source={{uri:this._getPhotoImage(5)}} style = {{width:photoSlotSize, height:photoSlotSize}}/>
                        </TouchableOpacity>
                    </View> */}

                    {/* 기초 정보 */}
                    <View style={style.baseInfoView}>
                        <View style={style.baseInfoTitle}>
                            <Text style= {style.textTitleMain}> 이름 </Text>
                        </View>
                        <View style={style.baseInfoContent}>
                            <Text style={style.textContent}> {myProfileTemp.memNm} </Text>
                        </View>
                    </View>
                    <View style={style.baseInfoView}>
                        <View style={style.baseInfoTitle}>
                            <Text style= {style.textTitleMain}> 성별 </Text>
                        </View>
                        <View style={style.baseInfoContent}>
                            <Text style={style.textContent}> {myProfileTemp.memGd === 'M' ? '남성' : '여성'}</Text>
                        </View>
                    </View>
                    <View style={style.baseInfoView}>
                        <View style={style.baseInfoTitle}>
                            <Text style= {style.textTitleMain}> 연령 </Text>
                        </View>
                        <View style={style.baseInfoContent}>
                            <Text style={style.textContent}> {myProfileTemp.age} </Text>
                        </View>
                    </View>

                    <View style={style.baseInfoView}>
                        <View style={style.baseInfoTitle}>
                            <Text style= {style.textTitleMain}> 지역 (필수) </Text>
                        </View>
                        <View style={style.baseInfoContent}>
                            <TextInput 
                                value={this.state.ctCd}
                                onChangeText = {(text) => {this.setState({ctCd:text})}}
                                style={style.textInput}
                                placeholder = {'입력해주세요.'}
                                underlineColorAndroid="transparent"/> 
                        </View>
                    </View>
                    <View style={style.baseInfoView}>
                        <View style={style.baseInfoTitle}>
                            <Text style= {style.textTitleMain}> 직업 (필수) </Text>
                        </View>
                        <View style={style.baseInfoContent}>
                            <TextInput 
                                value={this.state.job}
                                onChangeText = {(text) => {this.setState({job:text})}}
                                style={style.textInput}
                                placeholder = {'입력해주세요.'}
                                underlineColorAndroid="transparent"/> 
                        </View>
                    </View>
                    <View style={style.baseInfoView}>
                        <View style={style.baseInfoTitle}>
                            <Text style= {style.textTitleMain}> 직장 (선택) </Text>
                        </View>
                        <View style={style.baseInfoContent}>
                            <TextInput 
                                value={this.state.cmp}
                                onChangeText = {(text) => {this.setState({cmp:text})}}
                                style={style.textInput}
                                placeholder = {'입력해주세요.'}
                                underlineColorAndroid="transparent"/> 
                        </View>
                    </View>
                    <View style={style.baseInfoView}>
                        <View style={style.baseInfoTitle}>
                            <Text style= {style.textTitleMain}> 종교 (선택) </Text>
                        </View>
                        <View style={style.baseInfoContent}>
                            <TextInput 
                                value={this.state.rlgnCd}
                                onChangeText = {(text) => {this.setState({rlgnCd:text})}}
                                style={style.textInput}
                                placeholder = {'입력해주세요.'}
                                underlineColorAndroid="transparent"/> 
                        </View>
                    </View>
                    <View style={style.baseInfoView}>
                        <View style={style.baseInfoTitle}>
                            <Text style= {style.textTitleMain}> 성격 (선택) </Text>
                        </View>
                        <View style={style.baseInfoContent}>
                            <TextInput 
                                // onChangeText = {(text) => {this.setState({region:text})}}
                                style={style.textInput}
                                placeholder = {'입력해주세요.'}
                                underlineColorAndroid="transparent"/> 
                        </View>
                    </View>
                    <View style={style.baseInfoView}>
                        <View style={style.baseInfoTitle}>
                            <Text style= {style.textTitleMain}> 스타일 (선택) </Text>
                        </View>
                        <View style={style.baseInfoContent}>
                            <TextInput 
                                // onChangeText = {(text) => {this.setState({region:text})}}
                                style={style.textInput}
                                placeholder = {'입력해주세요.'}
                                underlineColorAndroid="transparent"/> 
                        </View>
                    </View>

                    {/* button */}
                    {/* <View style={{
                            justifyContent:'center',
                            alignItems:'center',
                            marginTop:25,
                        }}>
                            <TouchableOpacity onPress = {() => this._saveProfile()}>
                                <View style = {style.userInfoInputRegistButton}>
                                    <Image/>
                                    <Text style={style.userInfoInputRegistButtonText}> 등록하기 </Text>
                                </View>
                            </TouchableOpacity>
                        </View> */}
                </KeyboardAwareScrollView>    
            </View>
       );
   } 
}

const style = StyleSheet.create({
    mainView : {
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    baseInfoView : {
        flex:1,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },
    baseInfoTitle : {
        flex:2,
        alignItems:'flex-start',
        justifyContent:'center',
        // height : 30,
        margin : 15,
        marginBottom:8,
        // marginBottom : 3
    },
    baseInfoContent : {
        flex:3,
        alignItems:'flex-start',
        justifyContent:'center',
        flexDirection:'row'
    },
    layoutTitle : {
        flexDirection : 'row',
        // height : 30,
        alignItems : 'flex-end',
        margin : 15,
        // marginBottom : 3
    },
    textTitleMain : {
        fontSize : 20,
        color : 'rgb(145,199,219)',
        fontWeight : 'bold'
    },
    textTitleSub : {
        fontSize : 15,
        color : 'rgb(145,199,219)'
    },
    textContent : {
        flex:1,
        fontSize : 18
    },
    textInput : {
        borderBottomColor: 'rgb(240,240,240)',
        borderBottomWidth:3,
        flex:1,
        fontSize : 18,
        marginRight:15,
        paddingTop:3
    }, 
    photoView : {
        flexDirection : 'row',
        height : photoSlotSize+10,
        alignItems:'center',
        justifyContent:'center',
        marginTop:10,
        marginBottom:10,
        // padding:10
    },
    photo : {
        flex : 1,
        height : photoSlotSize,
        alignItems:'center',
        justifyContent:'center',
    },
    photoBorder : {
        borderWidth:2,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:15,
    },
    userInfoInputRegistButton:{
        justifyContent:'center',
        alignItems:'center',
        borderWidth:1,
        borderRadius:15,
        margin:20, 
        padding:10,
    },
    userInfoInputRegistButtonText:{
        color:'rgb(0,0,0)',
    },
});

const mapStateToProps = (state) => {
    return {
        isLogin: state.login.isLogin,
        myProfileTemp : state.profile.myProfileTemp,
        myProfile : state.profile.myProfile,
        userId : state.login.userId
    };
};

const mapDispatchProps = (dispatch) => {
    return {
        doLogout: () => {dispatch(actions.doLogout())},
        addUserPhotoImage: (num, photoImage) => {dispatch(actions.addUserPhotoImage(num, photoImage))},
        deletePhotoImage: (num) => {dispatch(actions.deleteUserPhotoImage(num))},
        setMyProfileTemp: () => {dispatch(actions.copyMyProfileInfo())},
        registProfile: (profileInfo, iamges, callback) => {dispatch(actions.registProfile(profileInfo, iamges, callback))},
        modifyProfile: (profileInfo, iamges, callback) => {dispatch(actions.modifyProfile(profileInfo, iamges, callback))},
        getUserProfileInfo : (id) => {dispatch(actions.getUserProfileInfo(id))},
    };
};

export default connect(mapStateToProps, mapDispatchProps)(MyProfile)