import React from 'react';
import {
    View,
    TextView,
    Image,
    StyleSheet
} from 'react-native';
import {connect} from 'react-redux';
import * as actions from '../../actions';

import { Constants, Contacts } from 'expo';

class Intro extends React.Component{
    componentDidMount() {
        // 전화번호부 접근 권한 신청
        setTimeout(() => this.askContactPermissionAsync(), 1);
        
        // intro 종료
        this.props.finishLoading()
      }
      
      async askContactPermissionAsync() {
        const permission = await Expo.Permissions.askAsync(Expo.Permissions.CONTACTS);
        if (permission.status !== 'granted') {
          console.log('contact permission is denied')
          return;
        }

        this.props.grantedContactPermission();

        const contacts = await Contacts.getContactsAsync({
        //   Contacts.PHONE_NUMBERS,
            fields: [
                Expo.Contacts.PHONE_NUMBERS,
                Expo.Contacts.EMAILS,
            ],
            pageSize: 1000,
            pageOffset: 0,
        });
        
        if (contacts && contacts.total > 0) {
            var contactInfo = [];

            contacts.data.map((contactData, index) => {
                data = {
                    name : contactData.name,
                    number : contactData.phoneNumbers && contactData.phoneNumbers.length > 0 ? contactData.phoneNumbers[0].number : ""
                }
                contactInfo.push(data);
            })
          
            this.props.saveContactInfo(contactInfo);
            console.log(contactInfo.length);
        }
      }

    render(){
        return(
            <View style={style.mainView}>
                <Image source={{uri : 'http://data.ygosu.com/upload_files/board_yeobgi/978920/55c5eaded84d5.jpg'}} />
            </View>
        )
    }
}

const style = StyleSheet.create({
    mainView:{
        flex:1
    }
})

const mapStateToProps = (state) => {
    return {
        isAppLoaded: state.intro.isAppLoaded
    };
};

const mapDispatchProps = (dispatch) => {
    return {
        saveContactInfo :(contactInfo) => {dispatch(actions.saveContactsInfo(contactInfo))},
        finishLoading: () => {dispatch(actions.finishApploading())},
        grantedContactPermission: () => {dispatch(actions.grantedContactPermission())}
    };
};

export default connect(mapStateToProps, mapDispatchProps)(Intro)