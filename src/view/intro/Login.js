import React, {PropTypes} from 'react';
import {View, 
    Text,
    TouchableHighlight, 
    StyleSheet, 
    Button, 
    TextInput,
    AsyncStorage,
    KeyboardAvoidingView,
    Image,
    TouchableOpacity,
    Keyboard
} from 'react-native';
import { connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {BaseComponent} from 'src-root/view/common/component/BaseComponent';
import CommonFunc from 'src-root/view/common/component/CommonFunc';
import { CheckBox } from 'react-native-elements'
import StringUtil from 'src-root/util/StringUtil'
import * as storageKey from 'src-root/util/AsyncStorageKey'

import * as actions from '../../actions';

class Login extends BaseComponent {

    static navigationOptions = ({navigation}) => {
        return {
            header:null
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            // username : "",//"111",
            // password : "",//"111",
            // isAutoLogin : true,
            // idSaved : true,
            ipAddress: "",
            keyboardSpace:0,
        };

        //for get keyboard height
        Keyboard.addListener('keyboardDidShow',(frames)=>{
            if (!frames.endCoordinates) return;
            this.setState({keyboardSpace: frames.endCoordinates.height-this.state.textInputY});
        });
        Keyboard.addListener('keyboardDidHide',(frames)=>{
            this.setState({keyboardSpace:0});
        });
    }

    componentWillUnmount(){
        Keyboard.removeAllListeners('keyboardDidShow');
        Keyboard.removeAllListeners('keyboardDidHide');
    }

    async _setDataStorage (key, value, callback) {

        var strValue = value;
        if (typeof value === 'boolean'){
            strValue = (value ? 'true' : 'false');
        }

        try {
            await AsyncStorage.setItem(storageKey.LOGIN_PREFIX+key, strValue, () => { AsyncStorage.flushGetRequests()});

            if (callback) callback();
          } catch (error) {
            // Error saving data
          }
    }

    async _getDataFromStorage (key, defaultData, callback) {
        try {
            const value = await AsyncStorage.getItem(storageKey.LOGIN_PREFIX+key);
            
            if (value){
                this.setState({[key] : StringUtil.getBooleanFromString(value)});
            }else{
                this.setState({[key] : defaultData});
            }

            if (callback) callback();
          } catch (error) {
            // Error retrieving data
          }
    }

    _getUserId = () => {
        return this.state.username
    }

    _initAfterSettingData = () => {
        
        if (this.state.isAutoLogin && this.state.username !== "" && this.state.password !== ""){
            // 자동 로그인
            var params = {
                username:this.state.username,
                password:this.state.password
            }

            var callback = () => {
                this.props.getUserProfileInfo({memId:this.state.username})
            }

            this.props.doLogin(params, callback);
        }
    }

    _initData = () => {
        var getData1 = () => this._getDataFromStorage (storageKey.IS_AUTO_LOGIN, true, this._initAfterSettingData);
        var getData2 = () => this._getDataFromStorage (storageKey.ID_SAVED, true, getData1);
        var getData3 = () => this._getDataFromStorage (storageKey.USERNAME, "", getData2);
        
        this._getDataFromStorage (storageKey.PASSWORD, "", getData3);
    }

    componentWillMount () {

        this._initData();
        // console.log("login : ", this.state);
    }

    _moveScreen = (screen) => {
        this.props.navigation.navigate(screen)
    }

    _exctueLogin = () => {
        
        var params = {
            username:this.state.username,
            password:this.state.password
        }

        var callback = () => {
            this.props.getUserProfileInfo({memId:this.state.username})
        }
        
        this.props.doLogin(params, callback);
    }

    _login = () => {

        if (this.state.username === ""){
            CommonFunc.showAlert("안내", "접속할 ID 를 입력해 주세요");
            return;
        }

        if (this.state.password === ""){
            CommonFunc.showAlert("안내", "패스워드를 입력해 주세요");
            return;
        }

        var setData1 = this._setDataStorage(storageKey.IS_AUTO_LOGIN, this.state.isAutoLogin, this._exctueLogin);
        var setData2 = this._setDataStorage(storageKey.USERNAME, this.state.username, setData1);
        this._setDataStorage(storageKey.PASSWORD, this.state.password, setData2);
        
    }

    render() {
        return (
            <View style = {[style.mainView, 
                {marginTop:-(this.state.keyboardSpace) , marginBottom:(this.state.keyboardSpace)}]}>
                <View style = {style.logo}>
                    <Image source={require("asset/logo.png")}/>

                    <Text style={{
                            position: 'absolute',
                            bottom: 0,
                            marginBottom:20, 
                            fontWeight:'bold',
                            fontSize:15,
                            color:'rgb(100, 100, 100)'
                        }}>
                        내 친구가 소개하는 믿을 수 있는 소개팅 
                    </Text>
                </View>

                <View style = {{flex : 1, width:'100%'}}>
                    <View style = {{flex : 1.3 , backgroundColor:'rgb(247, 83, 107)'}}>
                        <View style={{flex:1, justifyContent:'center',alignItems:'center', margin: 40, marginBottom:50}}>
                            <TextInput style={style.TextInput}  placeholder = {'ID를 입력해주세요.'} editable = {true} 
                                value = {this._getUserId()}
                                onChangeText = {(text) => this.setState({username: text})}
                                autoCapitalize="none"
                                autoCorrect={false}
                                underlineColorAndroid="transparent"/> 
                            <TextInput style={style.TextInput}  placeholder = {'비밀번호을 입력해주세요.'} editable = {true}
                                onChangeText = {(text) => this.setState({password: text})} secureTextEntry ={true}
                                autoCapitalize="none"
                                autoCorrect={false}
                                underlineColorAndroid="transparent"/> 
                        </View>
                        
                        <View style={style.textInputBottomStyle}>
                            <CheckBox
                                title='자동 로그인'
                                containerStyle={{backgroundColor:'transparent', borderWidth:0}}
                                textStyle={{color:'white'}}
                                checkedColor={'white'}
                                uncheckedColor={'white'}
                                checked={this.state.isAutoLogin}
                                onPress={() => this.setState({isAutoLogin: !this.state.isAutoLogin})}
                            />

                            <TouchableOpacity onPress = {() => {}}>
                                <Text style={{
                                    color:'white',
                                    fontWeight:'bold',
                                }}>
                                    아이디/비밀번호 찾기
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style = {{flex : 1, alignItems:'center', marginTop:20}}
                        ref="ref"
                        onLayout={({nativeEvent}) => {
                            this.refs.ref.measure((x, y, width, height, pageX, pageY) => {
                                this.setState({textInputY:y});
                            })
                        }}>
                        <View style={{
                            justifyContent:'center',
                            alignItems:'center',
                            // marginTop:25,
                            // marginBottom:15
                        }}>
                            <TouchableOpacity onPress = {() => this._login()}>
                                <View style = {style.loginButton}>
                                    <Text style={style.loginButtonText}> 로그인 </Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity style={{
                                marginTop:20
                                    // position: 'absolute',
                                    // bottom: 0, 
                                    // marginBottom:15
                                }} 
                                onPress = {() => this._moveScreen('UserRegist')}>
                            <Text style={style.userRegitButtonText}> 회원 가입 하기 </Text>
                        </TouchableOpacity>
                    </View>
                    {/* <View style={{flexDirection:'row', backgroundColor:'lightgreen'}}>
                        <TextInput style={{width:200, height:40}}  placeholder = {'TEST server ip'} editable = {true}
                            onChangeText = {(text) => this.setState({ipAddress: text})}
                            underlineColorAndroid="transparent"/> 
                        <Button title={"ip 저장"} onPress={() => this.props.setIpAddress(this.state.ipAddress)} />
                    </View>
                    <TextInput style={{width:300, height:40}}  placeholder = {'ID를 입력해주세요.'} editable = {true} 
                        value = {this._getUserId()}
                        onChangeText = {(text) => this.setState({username: text})}
                        autoCapitalize="none"
                        autoCorrect={false}
                        underlineColorAndroid="transparent"/> 
                    <TextInput style={{width:300, height:40}}  placeholder = {'비밀번호을 입력해주세요.'} editable = {true}
                        onChangeText = {(text) => this.setState({password: text})} secureTextEntry ={true}
                        autoCapitalize="none"
                        autoCorrect={false}
                        underlineColorAndroid="transparent"/> 
                    <Button style = {style.loginButton} title = {"Login"} onPress = {() => this._login()}/>
                    <Button style = {style.loginButton} title = {"Regist"} onPress = {() => this._moveScreen('UserRegist')}/>
                    <CheckBox
                        title='자동 로그인'
                        checked={this.state.isAutoLogin}
                        onPress={() => this.setState({isAutoLogin: !this.state.isAutoLogin})}
                        />
                    <CheckBox
                        title='ID 저장'
                        checked={this.state.idSaved}
                        onPress={() => this.setState({idSaved: !this.state.idSaved})}
                        /> */}
                </View>
            </View>
        );
    }
}

const style = StyleSheet.create({
    mainView : {
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    logo : {
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    }, 
    TextInput:{
        backgroundColor:'white',
        flex:1,
        width:'100%',
        margin:5,
        padding:5,
        borderRadius:8,
    },
    textInputBottomStyle:{
        position: 'absolute',
        bottom: 0, 
        paddingLeft:20,
        paddingRight:40,
        flex:1,
        width:'100%',
        backgroundColor:'transparent',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
    },
    // loginButton : {
    //     flex:1,
    //     alignItems:'center',
    //     justifyContent:'center'
    // },
    loginButton:{
        justifyContent:'center',
        alignItems:'center',
        borderWidth:1,
        borderRadius:15,
        padding:15,
    },
    loginButtonText:{
        color:'rgb(0,0,0)',
    },
    userRegitButtonText:{
        color:'rgb(100,100,100)',
        borderBottomWidth:1,
        borderBottomColor:'rgb(100,100,100)',
    }
});

const mapStateToProps = (state) => {

    return {
        isLogin: state.login.isLogin
    };
};

const mapDispatchProps = (dispatch) => {
    return {
        doLogin: (loginInfo, callback) => {dispatch(actions.doLogin(loginInfo, callback))},
        getUserProfileInfo : (id) => {dispatch(actions.getUserProfileInfo(id))},
        setIpAddress : (ipAddress) => {dispatch(actions.setIpAddress(ipAddress))}
        // getIntoRegist: () => {dispatch(actions.getIntoRegist())}
    };
};

export default connect(mapStateToProps, mapDispatchProps)(Login)