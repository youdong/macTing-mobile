import React from 'react';

import {
    View, 
    Text, 
    TextInput,
    StyleSheet,
    ScrollView,
    Button,
    Picker,
    Alert,
    Image,
    SafeAreaView,
    TouchableOpacity
} from 'react-native';

import moment from 'moment'
import DatePicker from 'react-native-datepicker'

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions';
import { MATCHING_SEARCH_MATCHING_INFO } from '../../actions';

import CommonFunc from 'src-root/view/common/component/CommonFunc';

const defaultInputTextHeight = 20;
const expandedInputTextHeight = 50;
const marginTop = 20;
const MIN_DATE = '1975-01-01';
var MAX_DATE = '2020-12-31';

const radioSelected = 'rgb(111,210,233)';
const radioUnSelected = 'rgb(100,100,100)';

class UserRegist extends React.Component {

    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel : '회원 가입',
            title : '회원 가입',
        }
    }    

    constructor(props) {
        super(props);
        this.state = {
            memId : "",
            pswd : "",
            pswdChk : "",
            memNm : "",
            memBd : "",
            memGd : "",
            cpNo : "",
            tempIdYn : "N",

            idHeight:defaultInputTextHeight,
            pwHeight:defaultInputTextHeight,
            pwCkHeight:defaultInputTextHeight,
            phoneNumHeight:defaultInputTextHeight,
            nameHeight:defaultInputTextHeight,
            birthHeight:defaultInputTextHeight,

            radioMale:radioUnSelected,
            radioTextMale:radioUnSelected,
            radioFemale:radioUnSelected,
            radioTextFemale:radioUnSelected,
        }
    }

    componentWillMount() {
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
        
        MAX_DATE = year+"-"+month+"-"+date
    }

    _testAlert = () => {
        Alert.alert(
            'Alert Title',
            '_setPhotoImage',
            [
              {text: 'Cancel'},
              {text: 'OK'},
            ],
            { cancelable: false }
          )
    }

    _expandTextInputHeight = (type) => {
        switch(type){
            case 0: // 아이디
                return  {
                    borderBottomColor: 'rgb(240,240,240)',
                    borderBottomWidth:3,
                    width:'100%',
                    height:this.state.idHeight
                }
            case 1: // 비밀번호
                return  {
                    borderBottomColor: 'rgb(240,240,240)',
                    borderBottomWidth:3,
                    width:'100%',
                    height:this.state.pwHeight
                }
            case 2: // 비밀번호 확인
                return  {
                    borderBottomColor: 'rgb(240,240,240)',
                    borderBottomWidth:3,
                    width:'100%',
                    height:this.state.pwCkHeight
                }
            case 3: // 전화번호
                return  {
                    borderBottomColor: 'rgb(240,240,240)',
                    borderBottomWidth:3,
                    width:'100%',
                    height:this.state.phoneNumHeight
                }
            case 4: // 이름
                return  {
                    borderBottomColor: 'rgb(240,240,240)',
                    borderBottomWidth:3,
                    width:'100%',
                    height:this.state.nameHeight
                }
            case 5: // 생년월일
                return  {
                    borderBottomColor: 'rgb(240,240,240)',
                    borderBottomWidth:3,
                    width:'100%',
                    height:this.state.birthHeight
                }
        }
    }

    _initHeight = () => {
        this.setState({idHeight: defaultInputTextHeight});
        this.setState({pwHeight: defaultInputTextHeight});
        this.setState({pwCkHeight: defaultInputTextHeight});
        this.setState({phoneNumHeight: defaultInputTextHeight});
        this.setState({nameHeight: defaultInputTextHeight});
        this.setState({birthHeight: defaultInputTextHeight});
    }

    _focusTextInput =  (type) => {
        this._initHeight();

        switch(type){
            case 0: // 아이디
                this.setState({idHeight: expandedInputTextHeight});
                break;
            case 1: // 비밀번호
                this.setState({pwHeight: expandedInputTextHeight});
                break;
            case 2: // 비밀번호 확인
                this.setState({pwCkHeight: expandedInputTextHeight});
                break;
            case 3: // 전화번호
                this.setState({phoneNumHeight: expandedInputTextHeight});
                break;
            case 4: // 이름
                this.setState({nameHeight: expandedInputTextHeight});
                break;
            case 5: // 생년월일
                this.setState({birthHeight: expandedInputTextHeight});
                break;
        }
    }

    _initRadioSelected = () => {
        this.setState({radioMale: radioUnSelected});
        this.setState({radioTextMale: radioUnSelected});
        this.setState({radioFemale: radioUnSelected});
        this.setState({radioTextFemale: radioUnSelected});
    }

    _onSelectedRadio = (selected) => {
        this._initRadioSelected();

        if (selected){
            // 남성 선택
            this.setState({radioMale: radioSelected});
            this.setState({radioTextMale: radioSelected});
            this.setState({memGd: 'M'});
        }else{
            // 여성 선택
            this.setState({radioFemale: radioSelected});
            this.setState({radioTextFemale: radioSelected});
            this.setState({memGd: 'F'});
        }
    }

    // id 중복 체크
    _onClickIdCheck = () => {
        console.log("id Check");
    }

    // 인증번호 요청
    _onClickAskNumber = () => {
        console.log("ask number");
    }

    // check for regist
    _checkForRegist = () => {
        if (this.state.memId === ""){
            CommonFunc.showAlert("안내", "Id를 입력해주세요");
            return false;
        }

        if (this.state.pswd === ""){
            CommonFunc.showAlert("안내", "비밀번호를 입력해주세요");
            return false;
        }

        if (this.state.pswdChk === ""){
            CommonFunc.showAlert("안내", "비밀번호 확인을 입력해주세요");
            return false;
        }

        if (this.state.pswd !== this.state.pswdChk){
            CommonFunc.showAlert("안내", "입력돤 비밀번호가 서로 상이합니다.");
            return false;
        }

        if (this.state.pswd === ""){
            CommonFunc.showAlert("안내", "비밀번호를 입력해주세요");
            return false;
        }

        if (this.state.cpNo === ""){
            CommonFunc.showAlert("안내", "전화번호를 입력해주세요");
            return false;
        }

        if (this.state.memNm === ""){
            CommonFunc.showAlert("안내", "이름을 입력해주세요");
            return false;
        }

        if (this.state.memBd === ""){
            CommonFunc.showAlert("안내", "생년월일을 입력해주세요");
            return false;
        }

        if (this.state.memGd === ""){
            CommonFunc.showAlert("안내", "성별을 선택해주세요");
            return false;
        }

        return true;
    }

    // 회원 등록
    _onClickRegist = () => {
        if(this._checkForRegist()){
            var userInfo = {
                memId:this.state.memId,
                pswd:this.state.pswd,
                memNm:this.state.memNm,
                memBd:this.state.memBd,
                memGd:this.state.memGd,
                cpNo:this.state.cpNo
            };
            var callback = () => {CommonFunc.showAlert('TEST', "regist succes")};
    
            this.props.registNewUser(userInfo, callback);
        }
    }

    render(){
        return (
            <SafeAreaView style = {style.main}>
                <ScrollView bounces={false}>
                    <View style={{flex:1, marginLeft:25, marginRight:25, marginTop:20}}>
                        {/* user info input */}
                        <View>
                            <View style={{marginTop:marginTop}}>
                                <View style={{flex:1, flexDirection:'row', height:20, alignItems: 'center'}}>
                                    <Text style = {style.userInfoInputTitleText} onPress = {()=>this._focusTextInput(0)}> 
                                        아이디 
                                    </Text>
                                    {/* 중복확인 버튼 */}
                                    <View style={{flex:1, alignItems: 'flex-end'}}>
                                        <TouchableOpacity onPress = {() => this._onClickIdCheck()}>
                                            <View style = {style.userInfoInputCheckButtonView}>
                                                <Image/>
                                                <Text style={style.userInfoInputCheckButton}> 중복체크 </Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View>
                                    <TextInput 
                                        onChangeText = {(text) => {this.setState({memId:text})}}
                                        style={this._expandTextInputHeight(0)}  onFocus = {()=>this._focusTextInput(0)}
                                        underlineColorAndroid="transparent"/> 
                                </View>
                            </View>
                            <View style={{marginTop:marginTop}}>
                                <View style={{flex:1, flexDirection:'row', height:20}}>
                                    <Text style = {style.userInfoInputTitleText}  onPress = {()=>this._focusTextInput(1)}> 
                                        비밀번호 
                                    </Text>
                                </View>
                                <View>
                                    <TextInput 
                                        onChangeText = {(text) => {this.setState({pswd:text})}}
                                        secureTextEntry
                                        style={this._expandTextInputHeight(1)}  onFocus = {()=>this._focusTextInput(1)}
                                        underlineColorAndroid="transparent"/> 
                                </View>
                            </View>
                            <View style={{marginTop:marginTop}}>
                                <View style={{flex:1, flexDirection:'row', height:20}}>
                                    <Text style = {style.userInfoInputTitleText} onPress = {()=>this._focusTextInput(2)}> 
                                        비밀번호 확인 
                                    </Text>
                                </View>
                                <View>
                                    <TextInput 
                                        onChangeText = {(text) => {this.setState({pswdChk:text})}}
                                        secureTextEntry
                                        style={this._expandTextInputHeight(2)}  onFocus = {()=>this._focusTextInput(2)}
                                        underlineColorAndroid="transparent"/> 
                                </View>
                            </View>
                            <View style={{marginTop:marginTop}}>
                                <View style={{flex:1, flexDirection:'row', height:20, alignItems: 'center'}}>
                                    <Text style = {style.userInfoInputTitleText} onPress = {()=>this._focusTextInput(3)}> 
                                        전화번호 
                                    </Text>
                                    {/* 번호인증 버튼 */}
                                    <View style={{flex:1, alignItems: 'flex-end'}}>
                                        <TouchableOpacity onPress = {() => this._onClickAskNumber()}>
                                            <View style = {style.userInfoInputCheckButtonView}>
                                                <Image/>
                                                <Text style={style.userInfoInputCheckButton}> 번호인증 </Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View>
                                    <TextInput 
                                        onChangeText = {(text) => {this.setState({cpNo:text})}}
                                        style={this._expandTextInputHeight(3)}  onFocus = {()=>this._focusTextInput(3)}
                                        underlineColorAndroid="transparent"/> 
                                </View>
                            </View>
                            <View style={{marginTop:marginTop}}>
                                <View style={{flex:1, flexDirection:'row', height:20}}>
                                    <Text style = {style.userInfoInputTitleText} onPress = {()=>this._focusTextInput(4)}> 
                                        이름 
                                    </Text>
                                </View>
                                <View>
                                    <TextInput 
                                        onChangeText = {(text) => {this.setState({memNm:text})}}
                                        style={this._expandTextInputHeight(4)}  onFocus = {()=>this._focusTextInput(4)}
                                        underlineColorAndroid="transparent"/> 
                                </View>
                            </View>
                            <View style={{marginTop:marginTop}}>
                                <View style={{flex:1, flexDirection:'row', justifyContent:'center', alignItems: 'center'}}>
                                    <Text style = {style.userInfoInputTitleText}> 생년월일 </Text>
                                    <View style={{flex:1, alignItems: 'flex-end'}}>
                                            {/* <View style={{flexDirection:'row'}}> */}
                                                {/* <TouchableOpacity onPress = {() => this._onSelectedRadio(true)}>
                                                    <View style = {[style.userInfoInputRadioButtonView, {borderColor:this.state.radioMale}]}>
                                                        <Text style={{color:this.state.radioTextMale}}> 남성 </Text>
                                                    </View>
                                                </TouchableOpacity>
                                                <TouchableOpacity onPress = {() => this._onSelectedRadio(false)}>
                                                    <View style = {[style.userInfoInputRadioButtonView, {borderColor:this.state.radioFemale}]}>
                                                        <Text style={{color:this.state.radioTextFemale}}> 여성 </Text>
                                                    </View>
                                                </TouchableOpacity> */}
                                                <DatePicker
                                                    style={{width: 200}}
                                                    date={this.state.memBd}
                                                    mode="date"
                                                    // placeholder="생년월일"
                                                    format="YYYY-MM-DD"
                                                    minDate={MIN_DATE}                                                    
                                                    maxDate={MAX_DATE}
                                                    confirmBtnText="Confirm"
                                                    cancelBtnText="Cancel"
                                                    showIcon={false}
                                                    customStyles={{
                                                    // dateIcon: {
                                                    //     position: 'absolute',
                                                    //     left: 0,
                                                    //     top: 4,
                                                    //     marginLeft: 0
                                                    // },
                                                    // dateInput: {
                                                    //     marginLeft: 36
                                                    // }
                                                    // ... You can check the source to find the other keys.
                                                    }}
                                                    onDateChange={(text) => {this.setState({memBd:text})}}
                                                />
                                    </View>
                                </View>
                                {/* <View>
                                    <TextInput 
                                        // onChangeText = {(text) => {this.setState({memBd:text})}}
                                        onChangeText = {(text) => {this._changeDateFormat(text)}}
                                        _changeDateFormat
                                        style={this._expandTextInputHeight(5)}  onFocus = {()=>this._focusTextInput(5)}/> 
                                </View> */}
                            </View>

                            <View style={{marginTop:marginTop}}>
                                <View style={{flex:1, flexDirection:'row', height:20, alignItems: 'center'}}>
                                    <Text style = {style.userInfoInputTitleText}> 성별 </Text>
                                    <View style={{flex:1, alignItems: 'flex-end'}}>
                                        <View style={{flexDirection:'row'}}>
                                            <TouchableOpacity onPress = {() => this._onSelectedRadio(true)}>
                                                <View style = {[style.userInfoInputRadioButtonView, {borderColor:this.state.radioMale}]}>
                                                    <Text style={{color:this.state.radioTextMale}}> 남성 </Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress = {() => this._onSelectedRadio(false)}>
                                                <View style = {[style.userInfoInputRadioButtonView, {borderColor:this.state.radioFemale}]}>
                                                    <Text style={{color:this.state.radioTextFemale}}> 여성 </Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>

                        {/* contract agree */}
                        <View>

                        </View>

                        {/* button */}
                        <View style={{
                            justifyContent:'center',
                            alignItems:'center',
                            marginTop:25,
                        }}>
                            <TouchableOpacity onPress = {() => this._onClickRegist()}>
                                <View style = {style.userInfoInputRegistButton}>
                                    <Image/>
                                    <Text style={style.userInfoInputRegistButtonText}> 약관 동의후 회원가입 </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

const style = StyleSheet.create({
    main : {
        flex:1,
        backgroundColor:'white'
    },
    userInfoInputContainer:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    userInfoInputTitleText:{
        color: 'rgb(111,210,233)',
        justifyContent:'center',
        alignItems: 'center'
    },
    userInfoInputCheckButton:{
        color: 'rgb(111,210,233)',
    }, 
    userInfoInputCheckButtonView:{
        borderColor: 'rgb(111,210,233)',
        borderWidth:1,
        borderRadius:15,
        padding:4,
        marginRight:20,
    },
    userInfoInputRadioButtonView:{
        borderWidth:1,
        borderRadius:5,
        padding:4,
        marginRight:5,
    },
    userInfoInputRegistButton:{
        justifyContent:'center',
        alignItems:'center',
        borderWidth:1,
        borderRadius:15,
        margin:20, 
        padding:10,
    },
    userInfoInputRegistButtonText:{
        color:'rgb(0,0,0)',
    },



    inputTable : {
        alignItems: 'center',
        justifyContent: 'center'
    },
    row: {
        height : 50,
        flexDirection: 'row',
    },
    title:{
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#7A00E5'
    },
    
    
    
    buttonView:{
        justifyContent : 'center',
        alignItems : 'center',
        height : 100,
        width : '100%',
        flexDirection : 'row'
    },
    button:{
        height : 70,
        width : 150,
        backgroundColor : 'blue',
        marginTop : 10,
        marginLeft : 5,
        marginRight : 5
    }
});

const mapStateToProps = (state) => {
    
        return {
            isLogin: state.login.isLogin
        };
    };
    
    const mapDispatchProps = (dispatch) => {
        return {
            doLogout: () => {dispatch(actions.doLogout())},
            registNewUser: (userInfo, callback) => {dispatch(actions.registNewUser(userInfo, callback))}
        };
    };
    
    export default connect(mapStateToProps, mapDispatchProps)(UserRegist)
