import React, {Component} from 'react'
import {BaseComponent} from 'src-root/view/common/component/BaseComponent'
import {View, Text, StyleSheet} from 'react-native'
import { Constants, Contacts } from 'expo';

export default class IntroduceFriend extends BaseComponent{
    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel : '주선하기',
            title:'주선하기',
            tabBarOnPress: (scene, jumpToIndex) => {
                if (!scene.focused){
                    jumpToIndex(scene.index);
                    
                    const navigationInRoute = scene.route;
                    if(!!navigationInRoute && !!navigationInRoute.params && !!navigationInRoute.params.selectedTab)
                        navigation.state.params.selectedTab();
                }
            },
        }
    }
      
    componentDidMount(){
        // 시간차를 주어서 세팅... 딱히 맘에 들지는 않는다 ㅠㅠㅠ
        setTimeout(() => {
            this.props.navigation.setParams({selectedTab: this._selectedTab.bind(this)});
          }, 1000)
    }

    _selectedTab = () => {
        console.log("Call server");
        //Code to make ajax call
      }
      
    //   async showFirstContactAsync() {
    //     const permission = await Expo.Permissions.askAsync(Expo.Permissions.CONTACTS);
    //     if (permission.status !== 'granted') {
    //       console.log('contact permission is denied')
    //       return;
    //     }

    //     const contacts = await Contacts.getContactsAsync([
    //       Contacts.PHONE_NUMBERS,
    //     ]);
    //     console.log('this is test!!!!',contacts.data.length)
    //     if (contacts.total > 0) {
    //       let contact = `Name: ${contacts.data[0].name}\n` +
    //         `Phone: ${JSON.stringify(contacts.data[0].phoneNumbers)}\n`;
            
    //         console.log(contact)
    //     }
    //   }

    render(){
        return(
            <View style = {style.mainView}>
                <Text> 여기는 친구 소개입니다. </Text>
            </View>
        );
    }
}

const style = StyleSheet.create({
    mainView : {
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    }
});