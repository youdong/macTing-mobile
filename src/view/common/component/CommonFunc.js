import React, {Component} from 'react'
import {View, 
    Text, 
    StyleSheet,
    Alert,
    Button,
    Modal
} from 'react-native'
import {commonStyles} from 'src-root/style/CommonStyle'

export default class CommonFunc extends Component{

    static showAlert = (title, content) => {
        Alert.alert(
            title,
            content,
            [
              {text: 'OK'},
            ],
            { cancelable: false }
          )
    }

    static showAlert1 = (btn) => {
        console.log("@!@!@!@!@")
    }
}