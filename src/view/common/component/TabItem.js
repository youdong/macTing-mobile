import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet
} from 'react-native'

export default class TabItem extends Component{
    _selectedTab = () => {
        console.log('Tab selected Tab typeof this.refs.child._selectedTab : ', typeof this.refs['child'], ", ", typeof this.refs['child']._selectedTab);

        if (this.refs['child']._selectedTab)
            this.refs['child']._selectedTab();
    }

    render (){
        const children = React.Children.map(this.props.children,
            (child, index) => {
                return React.cloneElement(child, {
                    ref : 'child'
                })
            }
        );

        const params = this.props
        return (
            <View title={params.thitle} style={{flex : 1}}>
                {children}
                {/* {React.cloneElement(this.props.children, {
                    ref : ref => this.child = ref
                })} */}
            </View>
        )
    }
}