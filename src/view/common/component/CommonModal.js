import React, {Component} from 'react'
import {View, 
    Text, 
    StyleSheet,
    Button,
    Modal,
    TouchableHighlight
} from 'react-native'

import {commonStyles} from 'src-root/style/CommonStyle'
import {Entypo} from '@expo/vector-icons'

export class CommonModal extends Component{

    constructor(props){
        super(props);
        this.state = {
            isVisibleModal: false,
        }
    }

    render() {
        return (
            <View> 
                <Modal
                    transparent={true}
                    visible={this.state.isVisibleModal}
                    onRequestClose={() => {
                        alert('Modal has been closed.');
                      }}
                    >
                    <TouchableHighlight style={{flex:1}}
                        onPress={() => {
                            this.setState({isVisibleModal:false})
                        }}>
                        <View style={commonStyles.modalBackground}>
                            <View style={commonStyles.modalContent}>
                                <View style={{flexDirection:'row'}}>
                                    <TouchableHighlight  style={[commonStyles.modalWitdh, {alignItems:'flex-end', marginTop:5, marginRight:10}]} 
                                        onPress = {() => this.setState({isVisibleModal:false})} >
                                        <Entypo
                                            size = {20} name={'cross'}/>
                                    </TouchableHighlight>
                                </View>
                                
                                <TouchableHighlight onPress = {() => {}}>
                                    {this.props.title ? <Text> {this.props.title} </Text> : <View/>}
                                </TouchableHighlight>
                                <TouchableHighlight onPress = {() => {}}>
                                    {this.props.body ? <Text> {this.props.body} </Text> : <View/>}
                                </TouchableHighlight>

                                {this.props.button.map((item, idx) => {
                                    return(
                                        <TouchableHighlight key={idx}
                                            onPress={item.func}>
                                            <Text style={commonStyles.modalText}>{item.btnNm}</Text>
                                        </TouchableHighlight>
                                    )
                                })}

                            </View>
                        </View>
                    </TouchableHighlight>
                </Modal>
            </View>
          );
    }

    showModal = () => {
        this.setState({isVisibleModal:true})
    }

    dismissModal = () => {
        this.setState({isVisibleModal:false})
    }
}