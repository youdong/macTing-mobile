import React, { Component } from 'react';
import {
    View,
  StyleSheet,         // CSS-like styles
  Text,               // Renders text
  TouchableOpacity,   // Pressable container
} from 'react-native';

export default class TabView extends Component {
    
    // Initialize State
    state = {
    // First tab is active by default
        activeTab: 0
    }

    _selectedTab = (index) => {
        this.setState({ activeTab: index })
        
        if (this.props.onSelectTab)
            this.props.onSelectTab(index);
        // if (this.refs['child']._selectedTab)
        //     this.refs['child']._selectedTab();
    }
   
    // Pull children out of props passed from App component
    // render({ children } = this.props) {
    render() {

        // const children = React.Children.map(this.props.children,
        //     (child, index) => {
        //         return React.cloneElement(child, {
        //             ref : 'child'
        //         })
        //     }
        // );

        const children = this.props.children;

        return (
            <View style={styles.container}>
                {/* Tabs row */}
                <View style={styles.tabsContainer}>
                {/* Pull props out of children, and pull title out of props */}
                    {children.map(({ props: { title } }, index) =>
                        <TouchableOpacity
                            style={[
                                // Default style for every tab
                                styles.tabContainer,
                                // Merge default style with styles.tabContainerActive for active tab
                                // index === this.state.activeTab ? styles.tabContainerActive : []
                            ]}
                            // Change active tab
                            onPress={() =>  this._selectedTab(index)}
                            // Required key prop for components generated returned by map iterator
                            key={index}
                        >
                            <Text style={[styles.tabText,
                                    index === this.state.activeTab ? styles.tabTextActive : styles.tabTextNonActive
                                ]}>
                                {title}
                            </Text>
                        </TouchableOpacity>
                        )
                    }
                </View>
                
                {/* Content */}
                <View style={styles.contentContainer}>
                    {children[this.state.activeTab]}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    // Component container
    container: {
      flex: 1,                            // Take up all available space
      borderBottomWidth:1,
      borderTopWidth:0.2
    },
    // Tabs row container
    tabsContainer: {
      flexDirection: 'row',               // Arrange tabs in a row
    //   paddingTop: 30,                     // Top padding
    },
    // Individual tab container
    tabContainer: {
      flex: 1,                            // Take up equal amount of space for each tab
      paddingVertical: 15,                // Vertical padding
      borderBottomWidth: 3,               // Add thick border at the bottom
      borderBottomColor: 'transparent',   // Transparent border for inactive tabs
      backgroundColor: '#FFFFFF',
      alignItems:'center',
      justifyContent:'center'
    },
    // Active tab container
    tabContainerActive: {
      borderBottomColor: '#555555',       // White bottom border for active tabs
    },
    tabTextActive: {
        color: '#000000',
        // padding:10,
        // borderBottomWidth:2,
        borderBottomColor:'rgb(141,197,218)'
    },
    tabTextNonActive: {
        color: 'rgb(190,190,190)'
    },
    // Tab text
    tabText: {
      fontWeight: 'bold',
    //   textAlign: 'center',
      flexWrap: 'wrap',
      alignItems:'center',
      justifyContent:'center'
    },
    // Content container
    contentContainer: {
      flex: 1,                             // Take up all available space
    }
  });