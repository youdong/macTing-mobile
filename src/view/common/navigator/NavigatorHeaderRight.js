import React, {Component} from 'react'
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native'
import {SimpleLineIcons} from '@expo/vector-icons'

// iconStyle 
// 1: drawerOpen, 2. back key

export default class NavigatorHeaderRight extends Component{
    render () {
        const {
            iconStyle,
            height,
            icon,
            text
        } = this.props

        return (
        <TouchableOpacity onPress = {this.props.action} >
            {icon ? <SimpleLineIcons style = {style.image} size = {height} name={icon}/> 
                    :<Text size = {height} > {text} </Text> }
        </TouchableOpacity>
        );
    }
}

const style = StyleSheet.create({
    image:{
        width : 20,
        height : 20,
        marginRight : 10,
    }
});