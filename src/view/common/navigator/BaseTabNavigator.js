import React, {Component} from 'react';
import {View, Text, Dimensions, Image} from 'react-native';
import {TabNavigator, TabBarBottom} from 'react-navigation';

import HeaderLeft from './NavigatorHeaderLeft';
import HeaderRight from './NavigatorHeaderRight';

import Matching from 'src-root/view/matching/Matching'
import IntroduceFriend from 'src-root/view/introduceFriend/IntroduceFriend'
import MyTing from 'src-root/view/myTing/MyTing'
import Community from 'src-root/view/community/Community'

const BaseTabNavigator  = TabNavigator({
    Matching :{
        screen : Matching,
        navigationOptions: ({navigation}) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                let iconName
                if (focused) return <Image source={require("asset/tab1_y.png")} style={{width:70, height:70, marginTop:15}}/>
                else return <Image source={require("asset/tab1_n.png")}  style={{width:70, height:70, marginTop:15}}/>
            },
            headerRight: <HeaderRight 
                height={19}
                icon={"list"}
                action={() => {
                    // navigation.navigate('DrawerOpen')
                }}
            />
        }),
    },
    IntroduceFriend:{
        screen : IntroduceFriend,
        navigationOptions: ({navigation}) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                let iconName
                if (focused) return <Image source={require("asset/tab2_y.png")} style={{width:70, height:70, marginTop:15}}/>
                else return <Image source={require("asset/tab2_n.png")}  style={{width:70, height:70, marginTop:15}}/>
            }
        })
    },
    MyTing :{
        screen : MyTing,
        navigationOptions: ({navigation}) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                let iconName
                if (focused) return <Image source={require("asset/tab3_y.png")} style={{width:70, height:70, marginTop:15}}/>
                else return <Image source={require("asset/tab3_n.png")}  style={{width:70, height:70, marginTop:15}}/>
            }
        })
    },
    Community :{
        screen : Community,
        navigationOptions: ({navigation}) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                let iconName
                if (focused) return <Image source={require("asset/tab4_y.png")} style={{width:70, height:70, marginTop:15}}/>
                else return <Image source={require("asset/tab4_n.png")}  style={{width:70, height:70, marginTop:15}}/>
            },
            headerRight: <HeaderRight 
                height={19}
                icon={"note"}
                action={() => {
                    navigation.navigate('Write')
                }}
            />
        })
    }
}, {
    initialRouteName: 'Matching',
    tabBarPosition:'bottom',
    swipeEnabled: true,
    tabBarOptions:{
        showIcon: true,
        // android setting
        style:{
            backgroundColor:'white',
        },
        tabBarSelectedItemStyle: {
            borderBottomWidth: 0,
            borderBottomColor: 'transparent',
            backgroundColor:'red'
        },
        iconStyle: {
            width: 40,
            height: 40,
            marginTop: 15,
            marginBottom:-15,
            padding:0       //Padding 0 here
        },
        // ios setting
        activeTintColor : 'rgb(248,122,141)',
        activeBackgroundColor : 'white',
        inactiveTintColor : 'black',
        inactiveBackgroundColor : 'white',
        labelStyle : {
            fontSize : 12,
            fontWeight:'bold'
        },
        tabStyle: {
            width: Dimensions.get('window').width/4,
            height:50,
        },
    },
    // navigationOptions:({ navigation }) => ({
    //     headerLeft:<HeaderLeft nav = {navigation}/>
    // })
});

export default BaseTabNavigator;