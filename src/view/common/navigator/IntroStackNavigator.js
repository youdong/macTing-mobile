import React, {Component} from 'react';
import {View, Platform, StatusBar} from 'react-native';
import {StackNavigator} from 'react-navigation';

import HeaderLeft from './NavigatorHeaderLeft';
import HeaderRight from './NavigatorHeaderRight';

import UserRegist from 'src-root/view/intro/UserRegist'
import Login from 'src-root/view/intro/Login'


const introStackNavigator = StackNavigator({
        Login: {
            screen: Login,
        },
        UserRegist: {
            screen: UserRegist,
        },
        
    },
    {
        navigationOptions: ({navigation}) => ({
            headerStyle: {
                marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
            },
            headerLeft: <HeaderLeft
                height={19}
                action={() => {
                    navigation.goBack();
                }}
                icon={"arrow-left"}
            />
        })
    }
);

export default introStackNavigator;