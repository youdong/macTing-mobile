import React, {Component} from 'react';
import {View, Platform, StatusBar} from 'react-native';
import {StackNavigator} from 'react-navigation';

import HeaderLeft from './NavigatorHeaderLeft';
import HeaderRight from './NavigatorHeaderRight';

import BaseTabNavigator from './BaseTabNavigator'
import BaseDrawerNavigator from './BaseDrawerNavigator'

import IntroduceFriend from 'src-root/view/introduceFriend/IntroduceFriend'
import Matching from 'src-root/view/matching/Matching'
import Profile from 'src-root/view/profile/Profile'
import MyTing from 'src-root/view/myTing/MyTing'
import Write from 'src-root/view/community/Write'
import Comments from 'src-root/view/community/Comments'

import MyProfile from 'src-root/view/sideMenu/myPage/MyProfile'

const BaseStackNavigator = StackNavigator({
        BaseDrawerNavigator: {
            screen: BaseDrawerNavigator
        },
        Profile: {
            screen: Profile,
            navigationOptions: ({navigation}) => ({
                headerLeft: <HeaderLeft
                    height={19}
                    action={() => {
                        navigation.goBack()
                    }}
                    icon={"arrow-left"}
                />
            })
        },
        Write: {
            screen: Write,
            navigationOptions: ({navigation}) => ({
                headerLeft: <HeaderLeft
                    height={19}
                    action={() => {
                        navigation.goBack()
                    }}
                    icon={"arrow-left"}
                />
            })
        },
        Comments: {
            screen: Comments,
            navigationOptions: ({navigation}) => ({
                headerLeft: <HeaderLeft
                    height={19}
                    action={() => {
                        navigation.goBack()
                    }}
                    icon={"arrow-left"}
                />
            })
        },
        
        MyProfile: {
            screen: MyProfile,
            navigationOptions: ({navigation}) => ({
                headerLeft: <HeaderLeft
                    height={19}
                    action={() => {
                        navigation.goBack()
                    }}
                    icon={"arrow-left"}
                />
            })
        },
    },
    {
        navigationOptions: ({navigation}) => ({
            headerLeft: <HeaderLeft
                height={19}
                action={() => {
                    navigation.navigate('DrawerOpen')
                }}
                icon={"user"}
            />,
            // headerRight: <HeaderRight 
            //     height={19}
            //     text={"TEST"}
            //     action={() => {
            //         navigation.navigate('DrawerOpen')
            //     }}
            // />,
            headerStyle: {
                marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
            }
        })
    }
);

export default BaseStackNavigator;