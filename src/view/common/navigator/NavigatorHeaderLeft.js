import React, {Component} from 'react'
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native'
import {SimpleLineIcons} from '@expo/vector-icons'

// iconStyle 
// 1: drawerOpen, 2. back key

export default class NavigatorHeaderLeft extends Component{
    render () {
        const {
            iconStyle,
            isOpenDrawer,
            height,
            icon
        } = this.props

        return (
        <TouchableOpacity onPress = {this.props.action} >
            <SimpleLineIcons style = {style.image} size = {height} name={icon}/>
        </TouchableOpacity>
        );
    }
}

const style = StyleSheet.create({
    image:{
        width : 20,
        height : 20,
        marginLeft : 10,
    }
});