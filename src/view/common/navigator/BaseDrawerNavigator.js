import React, {Component} from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {DrawerNavigator, DrawerItems, SafeAreaView} from 'react-navigation';

// import BaseStackNavigator from './BaseStackNavigator'
import BaseTabNavigator from './BaseTabNavigator'
import SideMenu from 'src-root/view/sideMenu/SideMenu'

import MyProfile from 'src-root/view/sideMenu/myPage/MyProfile'


const BaseDrawerNavigator = DrawerNavigator({
        BaseTabNavigator:{
            screen : BaseTabNavigator,
            navigationOptions:{
                drawerLockMode:'locked-closed'
            }
        },
        // MyProfile: {
        //     screen : MyProfile,
        // }
    }, 
    {
        contentComponent : (props) => (
                <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
                    <SideMenu {...props}/>
                </SafeAreaView>
        ),

        drawerOpenRoute: "DrawerOpen",
        drawerCloseRoute: "DrawerClose",
        drawerToggleRoute: "DrawerToggle"
    }
);

  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });
  

export default BaseDrawerNavigator;