import React, {Component} from 'react'
import {BaseComponent} from 'src-root/view/common/component/BaseComponent'
import {View,
    Text, 
    StyleSheet, 
    ScrollView,
    Image
} from 'react-native'
import SimpleCardView from './SimpleCardView'

import {commonColor} from 'src-root/style/CommonStyle'

import {connect} from 'react-redux';
import * as actions from '../../actions';

class MyTing extends BaseComponent{

    static navigationOptions = ({navigation}) => {
        return{
            tabBarLabel : '나의소개팅',
            title : '나의소개팅',
            tabBarOnPress: (scene, jumpToIndex) => {
                if (!scene.focused){
                    jumpToIndex(scene.index);
                    
                    const navigationInRoute = scene.route;
                    if(!!navigationInRoute && !!navigationInRoute.params && !!navigationInRoute.params.selectedTab)
                        navigation.state.params.selectedTab();
                }
            },
        }
    }

    _selectedTab = () => {
        //Code to make ajax call
      }

    componentDidMount(){
        // this.props.navigation.setParams({selectedTab: this._selectedTab.bind(this)});
        
        // 시간차를 주어서 세팅... 딱히 맘에 들지는 않는다 ㅠㅠㅠ
        setTimeout(() => {
            this.props.navigation.setParams({selectedTab: this._selectedTab.bind(this)});
          }, 1000)
    }

    render(){
        return(
            <View style = {style.mainView}>
                { (this.props.cardsWhoLikeMe && this.props.cardsWhoLikeMe.length > 0) ||
                (this.props.cardsWhoILike && this.props.cardsWhoILike.length > 0) ||
                (this.props.cardsWhoIAsk && this.props.cardsWhoIAsk.length > 0) ||
                (this.props.cardsWhoIInterest && this.props.cardsWhoIInterest.length > 0) ?
                    <ScrollView bounces={false}>
                        {/* 나를 좋아하는 카드 */}
                        { this.props.cardsWhoLikeMe && this.props.cardsWhoLikeMe.length > 0 ?
                            <View>
                                <Text style={style.titleText}> <Text style={{color:commonColor.pink}}>나를</Text> 좋아하는 카드 </Text>
                            
                                <SimpleCardView list={this.props.cardsWhoLikeMe} navigation={this.props.navigation}/>
                            </View>
                            :
                            <View/>
                        }

                        {/* 내가 좋아하는 카드 */}
                        { this.props.cardsWhoILike && this.props.cardsWhoILike.length > 0 ?
                            <View>
                                <Text style={style.titleText}> <Text style={{color:commonColor.pink}}>내가</Text> 좋아하는 카드 </Text>
                                <SimpleCardView list={this.props.cardsWhoILike} navigation={this.props.navigation}/>
                            </View>
                            :
                            <View/>
                        }

                        {/* 소개팅 주선을 요청한 카드 */}
                        { this.props.cardsWhoIAsk && this.props.cardsWhoIAsk.length > 0 ?
                            <View>
                                <Text style={style.titleText}> <Text style={{color:commonColor.pink}}>소개팅 주선</Text>을 요청한 카드 </Text>
                                <SimpleCardView list={this.props.cardsWhoIAsk} navigation={this.props.navigation}/>
                                </View>
                            :
                            <View/>
                        }

                        {/* 나의 관심카드 */}
                        { this.props.cardsWhoIInterest && this.props.cardsWhoIInterest.length > 0 ?
                            <View>
                                <Text style={style.titleText}> 나의 <Text style={{color:commonColor.pink}}>관심카드</Text> </Text>
                                <SimpleCardView list={this.props.cardsWhoIInterest} navigation={this.props.navigation}/>
                            </View>
                            :
                            <View/>
                        }
                    </ScrollView>
                    :
                    <View style = {{flex:1,
                            width:"100%",
                            height:"100%",
                            alignItems:'center',
                            justifyContent:'center'}}>
                        <Image style ={{
                            // flex:1,
                            width: '60%',
                            height :'60%',
                            resizeMode: Image.resizeMode.contain
                        }} 
                            source={require('asset/my_ting_empty.png')}/>
                        
                        <Text style={style.textStyle}> 소개팅 카드가 없습니다.</Text>
                        <Text style={style.textStyle}> 맥팅이 당신의 만남을 응원합니다! </Text>
                    </View>
                }
            </View>
        );
    }
}

const style = StyleSheet.create({
    mainView : {
        flex:1,
        backgroundColor:'white',
    },
    titleText : {
        fontSize : 20,
        color: '#72767c',
        marginLeft: 5,
        marginTop: 6,
        fontWeight:'bold'
    },
    textStyle:{
        fontSize:20,
        fontWeight:'bold',
        color: '#72767c',
    }

});

const mapStateToProps = (state) => {
    
        return {
            cardsWhoLikeMe: state.myTing.cardsWhoLikeMe,
            cardsWhoILike : state.myTing.cardsWhoILike,
            cardsWhoIAsk : state.myTing.cardsWhoIAsk,
            cardsWhoIInterest : state.myTing.cardsWhoIInterest
        };
    };
    
    const mapDispatchProps = (dispatch) => {
        return {
            searchMatchingInfo: () => {dispatch(actions.searchMatchInfo())},
        };
    };
    
    export default connect(mapStateToProps, mapDispatchProps)(MyTing)