import {View, 
    Text, 
    StyleSheet, 
    ScrollView,
    TouchableOpacity,
    Dimensions,
    Image,
    TouchableHighlight
  } from 'react-native';
import React from 'react';
import {connect} from 'react-redux';

import * as actions from '../../actions';

const DEFAULT_ITEM_COUNT_IN_ROW = 3
const DEFAULT_ITEM_MARGIN = 5

var photoSlotSize;// = Dimensions.get('window').width / DEFAULT_ITEM_COUNT_IN_ROW - (DEFAULT_ITEM_MARGIN * 2)

class SimpleCardView extends React.Component{

    constructor(props) {
        super(props);
        this.state ={
            itemCountInRow : DEFAULT_ITEM_COUNT_IN_ROW,
            itemList:[]
        }
      }

    componentWillMount(){
        if (this.props.itemCountInRow)
            this.state.itemCountInRow = this.props.itemCountInRow

        photoSlotSize = Dimensions.get('window').width / this.state.itemCountInRow - (DEFAULT_ITEM_MARGIN * 2)

        this._makeListForCardSet();
    }

    _makeListForCardSet(){
        if (this.props.list === null || this.props.list.length === 0 || this.props.list === 'undefined')
            return;
            
        const rowCount = this.props.list.length / this.state.itemCountInRow
        
        for (i=0; i<rowCount; i++){
            var subList = [];
            for (j=i*this.state.itemCountInRow; j < this.props.list.length; j++){
                subList.push(this.props.list[j]);
            }
            this.state.itemList.push(subList);
        }
    }

    _onPressButton(id) {
        this.props.navigation.navigate('Profile', { item: this.props.item, key:id});
    }

    render () {
        const {
          userInfo,
          navigation
        } = this.props;
        
        return (
            <View style={{flex:1, backgroundColor:'white'}}>
                {this.state.itemList.map((subItems, idx) =>
                    <View style={style.containView} key={idx}>
                        {subItems.map((item, idx) =>
                            <TouchableHighlight onPress={this._onPressButton.bind(this, idx)} underlayColor="white" key={idx}>
                                <View style={{
                                            alignItems:'flex-start',
                                            margin:DEFAULT_ITEM_MARGIN,
                                            marginBottom:10,
                                            borderWidth : 0.5,
                                            borderRadius : 10,
                                            paddingBottom : 10,
                                            borderColor : 'rgba(10,10,10,0.4)'
                                        }}>
                                    <View style={{width:photoSlotSize,height:photoSlotSize, alignItems:'center', justifyContent:'center',}}>
                                        <Image source={{uri:item.pictures[0].uri}} 
                                            style={[style.imageView, 
                                                    {width:photoSlotSize * 2 / 3,
                                                    height:photoSlotSize * 2 / 3,
                                                    borderRadius:photoSlotSize / 3,}]}/>
                                    </View>

                                    <View style = {{alignItems:'center',justifyContent:'center', width:'100%'}}>
                                        <Text style={style.contentsView}> {item.memNm} </Text>
                                        <Text style={style.contentsView}> {item.memAge} </Text>
                                    </View>
                                </View> 
                            </TouchableHighlight>      
                        )}
                    </View>
                )}
        </View>
        )
    };
}

const style = StyleSheet.create({
    containView:{
        flexDirection:'row',
        justifyContent:'flex-start'
    },
    imageView: {
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'transparent',
    },
    contentsView:{
        alignItems:'center',
        justifyContent:'center',
        fontSize:15,
        fontWeight:'bold'
    }
})

const mapStateToProps = (state) => {
    return {
        cardsWhoLikeMe: state.myTing.cardsWhoLikeMe,
        cardsWhoILike : state.myTing.cardsWhoILike
    };
};

const mapDispatchProps = (dispatch) => {
  return {
      doLogout: () => {dispatch(actions.doLogout())},
  };
};

export default connect(mapStateToProps, mapDispatchProps)(SimpleCardView)