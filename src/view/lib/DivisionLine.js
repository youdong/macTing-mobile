import React from 'react';
import { View, StyleSheet } from 'react-native'
import { BaseComponent } from 'src-root/view/common/component/BaseComponent'

export default class DivisionLine extends BaseComponent {
    
    render(){
        console.log("this.props : ", this.props);
        const _margin = this.props.margin === undefined ? 5 : this.props.margin;
        const _color = this.props.color === undefined ? 'black' : this.props.clolor;
        return (
            <View style={{borderTopWidth:1,margin: _margin, borderColor: _color}}></View>
        );
    }
} 