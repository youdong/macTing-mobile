import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native'
import { BaseComponent } from 'src-root/view/common/component/BaseComponent'

export default class ContentRow extends BaseComponent {
    render(){
        return (
            <View style={style.row}>
                <View style={style.head}>
                    <Text style={[style.contentText, {color:'black', marginLeft:10}]}>{this.props.title}</Text>
                </View>
                <View style={style.body}>
                    <Text style={[style.contentText, {color:'rgb(150,150,150)'}]}>{this.props.content}</Text>
                </View>
            </View>
        );
    }
} 

const style = StyleSheet.create({
    row: {
        flex:3,
        flexDirection: 'row'
    },
    head: {
        flex:1
    },
    body:{
        flex:2
    },
    contentText: {
        fontSize: 18,
        margin: 5,
        fontWeight:'bold'
    }
});