import {
    StyleSheet,
    Dimensions
} from 'react-native'

export const commonColor = {
    pink : 'rgb(248,122,141)',
    mint : 'rgb(145,199,219)'
}


export const commonStyles = StyleSheet.create({
    modalBackground:{
        flex:1, 
        backgroundColor:'rgba(230,230,230,0.7)', 
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalContent:{
        borderBottomColor:'rgb(0,0,0)',
        borderWidth:0.5,
        borderRadius:15,
        justifyContent: 'center',
        alignItems: 'center', 
        backgroundColor:'white'
    },
    modalText: {
        margin:12,
        width:Dimensions.get('window').width - 80,
        textAlign: 'center',
        fontSize:18,
        fontWeight:'bold'
    },
    modalWitdh: {
        width:Dimensions.get('window').width - 80,
    },
    roundImage:{
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:66,
        height:66,
        backgroundColor:'#fff',
        borderRadius:33,
    },
    roundImageSmall:{
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:40,
        height:40,
        backgroundColor:'#fff',
        borderRadius:20,
    },
});