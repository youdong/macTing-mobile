import {createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk'
import reducer from '../reducers';
import logger from '../middleware/loggerMiddleware'
import network from '../middleware/networkMiddleware'

export default createStore (
    reducer, 
    applyMiddleware(logger , network, thunkMiddleware)
);