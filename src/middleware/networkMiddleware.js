import {REQUEST_CALL, RECEIVE_CALL} from '../actions/common/networkAction'
import StringUtil from '../util/StringUtil'
import * as ENV from '../../env'

// import env from "env"
import {
  Alert
} from 'react-native'

export default function networkMiddleware({ dispatch, getState }) {
  _errorAlert = (title, msg) => {
    Alert.alert(
        title,
        msg,
        [
          {text: 'OK'},
        ],
        { cancelable: false }
      )
  }

    return function (next) {
      return function (action) {
        const {
          types,
          api,
          shouldCallAPI = () => true,
          payload = {},
          callback,
          params, 
          image,
          isLogin
        } = action;

        if (!types) {
          // Normal action: pass it on
          return next(action);
        }

        if (
          !Array.isArray(types) ||
          types.length !== 2 ||
          !types.every(type => typeof type === 'string')
        ) {
          throw new Error('Expected an array of three string types.');
        }

        if (!shouldCallAPI(getState())) {
          return;
        }

        // TO-DO : 추후에 변경
        // url = "http://localhost:9999/" + api;
        // exp://192.168.219.172:19000

        // url = "http://192.168.219.197:9999/" + api;
        // TEST_SERVER = 'http://192.168.219.197:9999';
        ip = ENV.SERVER;
        if (getState().login.ipAddress) ip = getState().login.ipAddress;

        url = ip + api;

        var header = {};  
        var body = "";
        var authPrefix = 'Bearer:';
        if (image) {      // image 전송용 (multipart)
          header = {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization':authPrefix+getState().login.token,
            'Client-Type':"mt"
          }

          body = new FormData();
          // body.append("files",{
          //   uri: image,
          //   name: 'my_photo.jpg',
          //   type: 'image/jpg'
          // })

          Object.keys(image).map( key => {
            value = image[key];
            // if (value instanceof Date) {
            if (value !== ''){
              body.append("files",{
                uri: value,
                name: 'my_photo'+key+'.jpg',
                type: 'image/jpg'
              })
            }
            // } else {
            //   body.append(key, String(value))
            // }
          })
          
          if (params){
            Object.keys(params).map( key => {
              value = params[key]
              if (value instanceof Date) {
                body.append(key, value.toISOString())
              } else {
                body.append(key, String(value))
              }
            })
          }
        }
        else{           // 일반 TR 전송용 header
          header = {
            'Accept':'application/json',
            'Content-Type':'application/x-www-form-urlencoded',
            'Authorization':authPrefix+getState().login.token,
            'Client-Type':"mt"
          }

          if (params){
            console.log('###params : ', params)
            body = StringUtil.getQueryStringFromMap(params);   // 일반 param 세팅
          }
        }

        console.log('###url : ', url)
        console.log('###header : ', header)
        console.log('###body : ', body)
        const [successType, failureType] = types;

        // indicator 작동
        dispatch({
          type: REQUEST_CALL
        });

        // timeout setting
        let timeout = 30000;
        let timeout_err = {
            ok: false,
            status: 408
        };

        let tiemout = () => setTimeout(reject.bind(null, timeout_err), timeout);

        new Promise(function(resolve, reject) {
          fetch(url, {
            method: "post",
            headers : header,
            body:body,
          })
          .then(
            (res) => {
              console.log("%%%%%%%%%%%%%%%% [1]",res);
              clearTimeout(tiemout); 
              if (res.status === 200){
                return response = res.json();
              }else{
                dispatch({type:RECEIVE_CALL})
                console.log("[NETWORK ERROR HTML ERROR] error code [",res.status,"]");
                _errorAlert("알림","네트워크를 확인해 주세요")
                dispatch({
                  type: failureType,
                })
              }
            },
          )
          .then(
            response => {
              console.log("%%%%%%%%%%%%%%%% [2]",response);
              if (response){
                if (response.resultCode === 200){         // 정상 수신!!!
                  dispatch({type:RECEIVE_CALL})
                  dispatch({
                    request: params,
                    response: response,
                    type: successType
                  })
                  if (callback) callback()
                }else{                                    // 서버에서 문제가 생겼을때... 404 등등               
                  dispatch({type:RECEIVE_CALL})
                  console.log("[NETWORK ERROR SERVER ERROR] error code [",response.resultCode,"] error msg [",response.resultMsg,"]")
                  _errorAlert("알림",response.resultMsg)
                    dispatch({
                      type: failureType,
                  })
                }
              }
            },
          ).catch(
            (err) => { 
                dispatch({type:RECEIVE_CALL})
                console.log("[NETORK ERROR CATCH ERROR] ",err);
              }
          );
          
          tiemout;      // timeout
        })
        // .then((response) => {
        //   console.log("%%%%%%%%%%%%%%%% [",response);
        //   return response.json()
        // },
        // (error) => {
        //   console.log("%%%%%%%%%%%%%%%% [",error);
        // }
        // )  
        // .then(
        //   response => {
        //     console.log(response)
        //     if (response.resultCode === 200){         // 정상 수신!!!
        //       dispatch({type:RECEIVE_CALL})
        //       dispatch({
        //         response: response,
        //         type: successType
        //       })
        //       if (callback) callback()
        //     }else{                                    // 서버에서 문제가 생겼을때... 404 등등               
        //       dispatch({type:RECEIVE_CALL})
        //       console.log("network catch error RECEIVE_CALL : ",response.resultMsg)
        //       _errorAlert("알림",response.resultMsg)
        //         dispatch({
        //           type: failureType,
        //       })
        //     }
        //   },
        //   error => {                                 // 네트워크에 문제가 생겼을 때... 
        //     dispatch({type:RECEIVE_CALL})
        //     console.log("network catch error : ",error)
        //     _errorAlert("알림","네트워크를 확인해 주세요")
        //     dispatch({
        //       error: error,
        //       type: failureType,
        //     })
        //   }
        // )
        .catch(
          (error) => { 
            dispatch({type:RECEIVE_CALL})
            console.log("[NETORK ERROR CATCH ERROR] ",error);

            if (error.status === 408){   // timeout
              pro.terminate();
              _errorAlert("알림","네트워크를 확인해 주세요 (TIMEOUT)");
            }else{
              _errorAlert("알림",JSON.stringify(error));
            }
        });
      };
      
    };
  }