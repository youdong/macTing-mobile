import React from 'react'
import {
    ImageEditor
} from 'react-native';
import { 
    ImagePicker 
} from 'expo';

const defaultPhotoIamge = 'http://cfile213.uf.daum.net/image/12654C355141A7CA2E5410';
const plusPhoto = 'asset/plus.png';
// const plusPhoto = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAN8AAADiCAMAAAD5w+JtAAAAe1BMVEX///8zMzMnJyefn58gICDx8fEMDAzExMQsLCwvLy8AAAApKSkcHBwZGRkfHx8kJCT5+fk5OTnQ0NB9fX3p6emnp6e1tbVRUVHX19e8vLxcXFxDQ0NiYmJ1dXWDg4OXl5eOjo49PT2IiIjV1dVhYWGvr69ubm6bm5vg4OD5tj85AAAE10lEQVR4nO2dCXbiMAxAY8hix9mAhEDYmSnl/iccaLpAWRqZoZb89C8Q/RcvshPLnvdLTMbrRb0ZiMGmGa2m+9967K9QjOdZ0tNSvKGkzhN/EdmO6n9RbnNfie/IRPQL26H9D9aZvpBryeWL7eAeZj/Ib9gdSZrAdoCPMY0vW+ZZK/UntkN8hCq+a3ckJjzOjMMf9YRIh7bDNCVKO+gJoUvbgZoR3Bo3v6FmtiM1o5Hd/ERvZDtUE8ZJRz0hQopdsFvna1vowHawcPq97n7Cp5fIQPSE2tgOF8o0g/jRmwTnXQfPFrmzHTCMokvmckpCa6009aF+tNLQJax5HpK0te2QQdT3l0WXqNp2yBAC6Os7CFLqgCV0eDnkaJRWERNAcvZOSmkhP7636XKdfGw7aACVgV9lO2gAFSj7fKNHyQ+0eHj369sOGgD7sR9m2I/9MMN+7IcZ9mM/zLAf+2GG/dgPM+zHfphhP/bDDPuxH2bYj/0ww37shxn2Yz/MsB/7YYb92A8z7Md+mGE/9sMM+7EfZtiP/TDDfuyHGQJ+RREYU6w6ltY4Qa8eeSDkcGQRVbtm5qehOfDXd3iBDzwvzWfNroo6WJb9Js60VNDzs7ZRSuosbvr36zlFTaypmZ2idDy/XQnhT51SlmuRaXO9OF4xcsDuiEzXV/T2G5NBASd5fdENI0deXotU39po9HOhMlKo8+PmQ8f0xHm9gBJ+HB89SnzN9jW8XAR+9GfJnJfulbwokU7fJz54MkyCj6pjfXgxBRpkbUkLlya+c95eYORm7zvyVnVs5OLg2aK3Bz9Xe98Rfcir3W2eQiR7L3L5/eWRZ7AVRAe9gldio4Rcen/dnf4OKczcm9mO4anMvIHtEJ7KwLMdwZNx3m9jOYDnMvBq2yE8lZm3cHn+O8wPa5fzF7k1KfVIB/3qTVxeP/iHBS6sEDctktJzeoCRbu+/vO1POLx/1la1f3V2BG03eAt3Pmye83GrRAUtFk+Fjy9IMye7YPpZcXoCLzeOH9l8feCsHPzAmZ/+Y7BwbgyNz//z2TkmeHFt28KpNCa8vA5k5c4fMFJd+wltKBxpo+nf638RFtvQgcV8rm/ftFCO4h7pVioTff8egqJfx7mk6KikTpMut+oG41EjwjBNMt+QzGTJLM0fl6ZhmNe7qvslX0VQToaRMQYf3eTS/HGTfRn86vU7BM4HPAT7sR9m2I/9MMN+7IcZ9mM/zLAf+2GG/dgPM+zHfphhP/bDDPuxH2bYj/0ww37shxn2Yz/MsB/7YYb92A8z7Md+mGE/9sMM+7EfZtiP/TDDfuyHGdf9KgO/++facFHBT4LmlPwMih7lt09d4uMPvF7AecF45JTwgg+n9eLRE8DPKcv7N6cgo4ae4FW17ZBBgItOy6XtkEGAB1B/ajtkEAF0gAlJdT/Pa2AdUM5tBwzkBdZAM1rN0/MK2Azfsx0vGFDdzZzS4qElgFzJk9iO1gDAtSAJpdz6k03XIfS0khchJl2HGFqp5xcv3QTT7mVNkLHtUpMrJNn5WpY/v8GYsJ7nVT/MEupKoTJSDNW9rbRsRmnVfpVind7KZHJSe4I3KZe5fzkVykS9Ep0XLgjG8yzp6fc1vZI6T/wulbwoMRmvF/VmIAazZrSaXr/C9Qn8A+eUew/YpjaeAAAAAElFTkSuQmCC";

export default class ImageUtil extends React.Component{
    static pickImageFromGalary = async(setImage, num) => {
        var result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [3, 3],
        });

        if (!result.cancelled) {
            // setImage(result.uri)
            
            // var thumbPhoto = 'data:image/jpeg;base64,' + result.data,
            //     photo = result.data,
            // re = /(?:\.([^.]+))?$/,
            // type = re.exec(result.uri);

            // if(type[1] === 'jpg' || type[1] === 'jpeg' || type[1] === 'png' )
            // {
            //     var imageObj = {thumbPhoto: thumbPhoto, photo: photo};
            //         // this.setState(imageObj);
            //         // this.props.updateImage(imageObj);
            // }
            // else{
            //     Alert.alert(
            //         'Invalid Image Type. Please select a non GIF image.',
            //         '',
            //         [
            //             {text: 'Ok', onPress: () => console.log('no gifs please')},
            //         ]
            //     );
            // }

            var wantedMaxSize = 1280;
            var rawheight = result.height;
            var rawwidth = result.width;
            
            var ratio = rawwidth / rawheight;
            // check vertical or horizont
            if(rawheight > rawwidth){
                var wantedwidth = wantedMaxSize*ratio;
                var wantedheight = wantedMaxSize;
            }
            else {
                var wantedwidth = wantedMaxSize;
                var wantedheight = wantedMaxSize/ratio;
            }
            
            let resizedUri = await new Promise((resolve, reject) => {
                ImageEditor.cropImage(result.uri,
                    {
                        offset: { x: 0, y: 0 },
                        size: { width: wantedwidth, height: wantedheight },
                        displaySize: { width: wantedwidth, height: wantedheight },
                        resizeMode: 'contain',
                    },
                    (uri) => resolve(uri),
                    () => reject(),
                );
            }).catch((error) => { 
                console.log("[ERROR] ",error)
            })
            
            console.log("EERERE ", num, ", ", result, ", ", setImage);
            if (num) setImage (num, result.uri);
            else setImage(result.uri);

            // this.setState({ 
            //     image: resizedUri
            // });
        }
    };

    static getDefaultImage = () => {
        return defaultPhotoIamge;
    };

    static getPlusImage = () => {
        return plusPhoto;
    }

    static checkImageUri = (imageUri) => {
        if (imageUri === defaultPhotoIamge) return "";
        if (imageUri === plusPhoto) return "";
        
        return imageUri;
    }
}