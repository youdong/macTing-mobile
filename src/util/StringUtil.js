import React from 'react'

export default class StringUtil extends React.Component{
    static getQueryStringFromMap = (map) => {
        var result = "";

        Object.keys(map).map( key => {
            value = map[key];
            if (result === "") {
                // result = encodeURIComponent(key) + "=" + encodeURIComponent(value);
                result = (key) + "=" + (value);
            } else{
                // result = result + '&' + encodeURIComponent(key) + "=" + encodeURIComponent(value);
                result = result + '&' + (key) + "=" + (value);
            }
        })

        return result;
    }

    static getTagExpFromArray = (arr) => {
        if(!Array.isArray(arr) ) return;

        var result = "";

        arr.map((item, idx) => {
            if (item.tagNm){
                result = result + '#' + item.tagNm + '  '
            }
        })

        return result;
    }

    static getTagExpFromArray2 = (arr) => {
        if(!Array.isArray(arr) ) return;

        var result = "";

        arr.map((item, idx) => {
            if (item){
                result = result + '#' + item + '  '
            }
        })

        return result;
    }

    static getBooleanFromString = (value) => {
        if (value === 'true' || value === 'false'){
            if (value === 'true'){
                return true;
            }else if  (value === 'false'){
                return false;
            }
        }
        
        return value;
    }

    static getStringFromBoolean = (value) => {
        if (value => typeof boolean === true){
            return value.toString();
        }

        return value;
    }

    static getPhoneNumWithFormat = (phoneNum) => {
        if(phoneNum){
            if (phoneNum.length === 11){
                return phoneNum.substr(0, 3) + "-" + phoneNum.substr(3, 4) + "-" + phoneNum.substr(7, phoneNum.length)
            }else{
                return phoneNum.substr(0, 3) + "-" + phoneNum.substr(3, 3) + "-" + phoneNum.substr(6, phoneNum.length)
            }
        }
    }

    static makePicArray = (pic1, pic2, pic3) => {
        var result = []
        if (pic1 && pic1 !== '')
            result.push(pic1);
        if (pic2 && pic2 !== '')
            result.push(pic2);
        if (pic3 && pic !== '')
            result.push(pic3);

        return result;
    }
}