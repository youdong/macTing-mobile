export const LOGIN_PREFIX = "login.";

export const USERNAME = 'username';
export const PASSWORD = 'password';
export const IS_AUTO_LOGIN = 'isAutoLogin';
export const ID_SAVED = 'idSaved';